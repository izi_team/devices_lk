#ifndef _DEV_TEMP_HUM_AM2320_H_
#define _DEV_TEMP_HUM_AM2320_H_

#define FUNC_READ_REG   0x03

typedef struct temp_hum_am2320_device {
    uint8_t bus_id;
    uint8_t i2c_address;
    int    (*read) (struct temp_hum_am2320_device *, float *, float *);
} temp_hum_am2320_device_t;

void temp_hum_am2320_init(temp_hum_am2320_device_t *device,
                          uint8_t bus,
                          uint8_t device_address);

#endif  // _DEV_TEMP_HUM_AM2320_H_
