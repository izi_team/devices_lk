/*
 * Copyright (c) 2020 Madalin CHERCIU
 * Copyright (c) 2020 IZITRON (Mihail CHERCIU)
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <stdlib.h>
#include <app.h>
#include <string.h>
#include <dev/i2c.h>
#include <lk/trace.h>
#include <platform.h>
#include <kernel/thread.h>
#include "am2320.h"

#define LOCAL_TRACE 0

static int temp_hum_am2320_read(temp_hum_am2320_device_t *device, void *data, uint8_t len) {
    int ret;

    ret = i2c_receive(device->bus_id, device->i2c_address, data, len);

    if (ret) TRACEF("Fail to read from am2320: %d\n", ret);

    return ret;
}

static int temp_hum_am2320_write(temp_hum_am2320_device_t *device, const void *data, uint8_t len) {
    int ret;

    ret = i2c_transmit(device->bus_id, device->i2c_address, data, len);

    return ret;
}

static int temp_hum_am2320_getValue(temp_hum_am2320_device_t *device, float *hum, float *temp) {
    uint8_t registers[3] = {FUNC_READ_REG, 0x00, 0x04};
    uint8_t data[8];
    int ret;
    uint8_t retry = 0;

    do {
        ret = temp_hum_am2320_write(device, registers, sizeof(registers));
        thread_sleep(250);
        retry++;
        if (retry > 10) break;
    } while (ret);
    if (ret) {
        if (ret) TRACEF("Fail to write to am2320: %d\n", ret);
        return ret;
    }

    ret = temp_hum_am2320_read(device, data, sizeof(data));
    if (ret) return ret;

    *hum = (float) (data[2] << 8 | data[3]) / (float)10.0;

    uint16_t temp_temperature = (data[4] << 8 | data[5]);

    if (temp_temperature & 0x8000) {

        temp_temperature = -(int16_t) (temp_temperature & 0x7fff);

    } else {

        temp_temperature = (int16_t) temp_temperature;

    }

    *temp = (float) temp_temperature / (float)10.0;

    return ret;
}

void temp_hum_am2320_init(temp_hum_am2320_device_t *device,
                          uint8_t bus,
                          uint8_t device_address)
{
    device->bus_id       = bus;
    device->i2c_address  = device_address;
    device->read         = &temp_hum_am2320_getValue;
}
