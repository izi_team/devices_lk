/*
 * Copyright (c) 2024 Ali CHOUCHENE
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <stdlib.h>
#include <app.h>
#include <string.h>
#include <platform/i2c.h>
#include <lk/trace.h>
#include <kernel/thread.h>
#include <hardware/gpio.h>
#include "ssd1316.h"
#include "font8x8_basic.h"

#define LOCAL_TRACE 0


/*################################# Functions for i2c communication #################################*/

static void ssd1316_write(ssd1316_device_t * device,
                               uint8_t* data,
                               size_t len) {
    int ret;

    ret = i2c_transmit(device->bus_id,device->i2c_address, data, len);
    if (ret) TRACEF("Write to FLASH fail: %d\n", ret);
}

static void command(ssd1316_device_t * device, uint8_t cmd){

    // data to be send : cmd command code + cmd
    uint8_t data[2]={SSD_CMD,cmd};
    // send command to ssd1316 module
    ssd1316_write(device,data,2);
}

static void data(ssd1316_device_t * device,uint8_t value){

    // data to be send : data command code + data
    uint8_t data[2]={SSD_RAM,value};
    // send data to ssd1316 module
    ssd1316_write(device,data,2);
}


void ssd1316_begin(ssd1316_device_t * device){
    
    // Start sequence
    gpio_put(device->rst_pin, 0);
    thread_sleep(1);
    gpio_put(device->rst_pin, 1);
    thread_sleep(1);
    
    command(device,SSD_CHARGE_CMD_PUMP_ENABLE);         // set charge pump enable
    command(device,SSD_CMD_ENABLE);                     // enable
    command(device,SSD_CMD_SET_CLOCK_DEVIDE_RATIO);     // set display clock divide ratio/oscillator frequency
    command(device,0xF1);
    command(device,SSD_CMD_TURN_ON_OLED_PANEL);         // turn on oled panel
}

void ssd1316_display_image(ssd1316_device_t * device, uint8_t* buf){

    uint8_t page,i;

    for(page=0;page<PAGES;page++){
        command(device,SSD_CMD_SET_PAGE_ADRESS + page); // set page adress
        command(device,0x00);                           // set high column adress
        command(device,0x10);                           // set low column address

        for(i=0;i<WIDTH;i++)
            data(device,buf[i+page*WIDTH]);             // write each pixel        
    }
}

/* Write a single charachter on the page */
static void ssd1316_put_char(ssd1316_device_t * device, uint8_t text_char) {
    
    for(int i=MAX_FONT_SIZE-1; i>=0; i--){
        data(device,font8x8_basic_tr[text_char][i]);
    }
}

/* erase page */
void ssd1316_erase_page(ssd1316_device_t * device, uint8_t page) {
    
    // set page
    command(device,SSD_CMD_SET_PAGE_ADRESS + page); // set page adress
    command(device,0x00); // set high column adress
    command(device,0x10); // set low column address

    // erasing page by putting spaces equivalent empty pixel
    for(int i=0; i< SSD1316_MAX_PAGE_CHRACHTERS; i++) {
        ssd1316_put_char(device,' ');
    }
}

/* Write a text on a given page */
void ssd1316_display_text(ssd1316_device_t * device, uint8_t page, char* text, int len) {

    int i; 

    if(len > SSD1316_MAX_PAGE_CHRACHTERS) {
        return;
    }

    command(device,0x00); // set high column adress
    command(device,0x10); // set low column address
    
    // set page to display text
    command(device,SSD_CMD_SET_PAGE_ADRESS + page); // set page adress

    // add more spaces to complete the page
    for(i = 0; i<SSD1316_MAX_PAGE_CHRACHTERS - len; i++) {
        ssd1316_put_char(device, ' ');
    }
    
    // display text
    for(i=len-1; i >= 0; i--) {
        ssd1316_put_char(device, text[i]);
    }
}

/* Start screen scrolling */
void ssd1316_scroll_on(ssd1316_device_t * device){

    command(device,0x26); // right horizontal scroll
    command(device,0x00); // dummy byte
    command(device,0x00); // define page 0 as start page adress
    command(device,0x00); // set time intervall to 5 frames
    command(device,0x03); // define page 3 as end of page
    command(device,0x00); // dummy byte
    command(device,0x00); // define column 0 as start cloumn
    command(device,0xFF); // define 127 as end column address
    command(device,0x2F); // activate scrolling
}

/* Stop screen scrolling */
void ssd1316_scroll_off(ssd1316_device_t * device) {

    command(device,0x2E); // turn off scrolling
}

/* Erase screen */
void ssd1316_erase_screen(ssd1316_device_t * device){

    for(int page=0;page<PAGES;page++) {
        ssd1316_erase_page(device, page);           // erase page
    }
}

/* Start inverting pixels (white pixels become black and vice versa)*/
void ssd1316_invert_on(ssd1316_device_t * device){

    command(device,SSD_CMD_INVERT_ON);              // Invert on 

}

/* Stop inverting pixels */
void ssd1316_invert_off(ssd1316_device_t * device){

    command(device,SSD_CMD_INVERT_OFF);             // Invert off

}

/* Fase out*/
void ssd1316_fade(ssd1316_device_t * device){
    
    command(device,SSD_CMD_SET_FADE_AND_BLINKING); // Send fade and blinking command
    command(device,SSD_CMD_SET_FADE_OUT);          // Set fade out

}

/* Turn off the */
void ssd1316_stop(ssd1316_device_t * device){
    
    command(device,SSD_CMD_DISPLAY_OFF);          // Set display off
    command(device,SSD_CMD_CHARGE_PUMP_SETTIING); // Charge pump setting command
    command(device,SSD_CMD_DISABLE_CHARGE_PUMP);  // Disable charge pump
}

/* Init the ssd1316 controller */
void ssd1316_init(ssd1316_device_t * device,
                        uint8_t bus,
                        uint8_t rst_pin,
                        uint8_t device_address)
{
    device->bus_id                  = bus;
    device->rst_pin                 = rst_pin;
    device->i2c_address             = device_address;
    device->ssd1316_begin           = &ssd1316_begin;
    device->ssd1316_display_image   = &ssd1316_display_image;
    device->ssd1316_erase_screen    = &ssd1316_erase_screen;
    device->ssd1316_invert_on       = &ssd1316_invert_on;
    device->ssd1316_invert_off      = &ssd1316_invert_off;
    device->ssd1316_scroll_on       = &ssd1316_scroll_on;
    device->ssd1316_scroll_off      = &ssd1316_scroll_off;
    device->ssd1316_fade            = &ssd1316_fade;   
    device->ssd1316_stop            = &ssd1316_stop; 
    device->ssd1316_display_text    = &ssd1316_display_text;
    device->ssd1316_erase_page      = &ssd1316_erase_page;
}
