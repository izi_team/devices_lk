#ifndef SSD1316_H
#define SSD1316_H

#include <target/gpioconfig.h>


#define WIDTH       128
#define HEIGHT      32
#define PAGES       HEIGHT/8

#define SSD_CMD     0x00
#define SSD_RAM     0x45

// Predefined SSD1316 commands

/* Commands upcode*/
#define SSD_CMD_SET_FADE_AND_BLINKING       0x23
#define SSD_CMD_SET_FADE_OUT                0x21
#define SSD_CMD_SET_PAGE_ADRESS             0xB0
#define SSD_CMD_SET_CLOCK_DEVIDE_RATIO      0xD5

/* Setting command*/
#define SSD_CMD_INVERT_ON                   0xA7
#define SSD_CMD_INVERT_OFF                  0xA6
#define SSD_CHARGE_CMD_PUMP_ENABLE          0x8D
#define SSD_CMD_ENABLE                      0x14
#define SSD_CMD_TURN_ON_OLED_PANEL          0xAF
#define SSD_CMD_DISPLAY_OFF                 0xAE
#define SSD_CMD_CHARGE_PUMP_SETTIING        0x8D
#define SSD_CMD_DISABLE_CHARGE_PUMP         0x10

/* Global definitions*/
#define SSD1316_MAX_PAGE_CHRACHTERS          16
#define MAX_FONT_SIZE                        8

/**
 * @brief       Structure representing an SSD1316 OLED device.
 * 
 * @details     This structure encapsulates configuration parameters and function pointers 
 *              for controlling an SSD1316 OLED display. It includes methods for initialization, 
 *              image display, text rendering, and various display effects.
 */
typedef struct ssd1316_device {
    /**
     * @brief   Identifier for the communication I2C bus.
     */
    uint8_t bus_id;

    /**
     * @brief   GPIO pin used for resetting the SSD1316 device.
     */
    uint8_t rst_pin;

    /**
     * @brief   I2C address of the SSD1316 device.
     */
    uint8_t i2c_address;

    /**
     * @brief   Function to initialize the SSD1316 device.
     * 
     * @param   device  Pointer to the SSD1316 device structure.
     * @return  void
     */
    void (*ssd1316_begin)(struct ssd1316_device *);

    /**
     * @brief   Function to display an image on the SSD1316 screen.
     * 
     * @param   device  Pointer to the SSD1316 device structure.
     * @param   data    Pointer to the image data buffer.
     * @return  void
     */
    void (*ssd1316_display_image)(struct ssd1316_device *, uint8_t *);

    /**
     * @brief   Function to enable display inversion (white pixels become black and vice versa).
     * 
     * @param   device  Pointer to the SSD1316 device structure.
     * @return  void
     */
    void (*ssd1316_invert_on)(struct ssd1316_device *);

    /**
     * @brief   Function to disable display inversion.
     * 
     * @param   device  Pointer to the SSD1316 device structure.
     * @return  void
     */
    void (*ssd1316_invert_off)(struct ssd1316_device *);

    /**
     * @brief   Function to enable scrolling of the display content.
     * 
     * @param   device  Pointer to the SSD1316 device structure.
     * @return  void
     */
    void (*ssd1316_scroll_on)(struct ssd1316_device *);

    /**
     * @brief   Function to disable scrolling of the display content.
     * 
     * @param   device  Pointer to the SSD1316 device structure.
     * @return  void
     */
    void (*ssd1316_scroll_off)(struct ssd1316_device *);

    /**
     * @brief   Function to apply a fade effect to the display.
     * 
     * @param   device  Pointer to the SSD1316 device structure.
     * @return  void
     */
    void (*ssd1316_fade)(struct ssd1316_device *);

    /**
     * @brief   Function to stop all display activity and turn off the screen.
     * 
     * @param   device  Pointer to the SSD1316 device structure.
     * @return  void
     */
    void (*ssd1316_stop)(struct ssd1316_device *);

    /**
     * @brief   Function to display a string of text on the SSD1316 screen.
     * 
     * @param   device  Pointer to the SSD1316 device structure.
     * @param   line    Line number where the text will be displayed.
     * @param   text    Pointer to the string of text to display.
     * @param   len     Length of the text to display.
     * @return  void
     */
    void (*ssd1316_display_text)(struct ssd1316_device *, uint8_t, char *, int);

    /**
     * @brief   Function to erase a specific page on the SSD1316 screen.
     * 
     * @param   device  Pointer to the SSD1316 device structure.
     * @param   page    Page number to erase.
     * @return  void
     */
    void (*ssd1316_erase_page)(struct ssd1316_device_t *, uint8_t);

    /**
     * @brief   Function to erase the entire SSD1316 screen.
     * 
     * @param   device  Pointer to the SSD1316 device structure.
     * @return  void
     */
    void (*ssd1316_erase_screen)(struct ssd1316_device_t *);
} ssd1316_device_t;


void ssd1316_init(ssd1316_device_t * device,
                        uint8_t bus,
                        uint8_t rst_pin,
                        uint8_t device_address);


#endif  // SSD1316_H