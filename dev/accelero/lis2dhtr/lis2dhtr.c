/*
 * Copyright (c) 2020 Mihail CHERCIU
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <stdlib.h>
#include <app.h>
#include <string.h>
#include <dev/gpio.h>
#include <platform/spi.h>
#include <lk/trace.h>
#include "lis2dhtr.h"

#define LOCAL_TRACE 0

float sensitivity = SCALE_SENSITIVITY_2G;
int   global_scale= 2;
uint8_t   global_data_rate;
static void acc_lis2dhtr_write(acc_lis2dhtr_device_t * device,
                               uint8_t reg,
                               uint8_t value) {
    int ret;
    uint8_t send_data[2];

    send_data[0] = reg;
    send_data[1] = value;

    gpio_set(device->nss_pin, 0);
    ret = spi_transmit(device->bus_id, send_data, 2);
    if (ret) TRACEF("Write to FLASH fail: %d\n", ret);
    gpio_set(device->nss_pin, 1);
}

static uint8_t acc_lis2dhtr_read(acc_lis2dhtr_device_t * device,
                              uint8_t reg) {
    int ret;
    uint8_t send_data[1];
    uint8_t rec_data[1];

    send_data[0] = reg | 0x80;

    gpio_set(device->nss_pin, 0);
    ret = spi_transmit(device->bus_id, send_data, 1);
    if (ret) TRACEF("Write to FLASH fail: %d\n", ret);
    ret = spi_receive(device->bus_id, rec_data, 1);
    if (ret) TRACEF("Read from FLASH fail: %d\n", ret);
    gpio_set(device->nss_pin, 1);

    return rec_data[0];
}

static uint8_t acc_lis2dhtr_get_id(acc_lis2dhtr_device_t * device) {
    uint8_t id_acc;

    id_acc = acc_lis2dhtr_read(device, LIS2DH_WHO_AM_I);
    return id_acc;
}

static void acc_lis2dhtr_set_lowpower(acc_lis2dhtr_device_t * device, bool state) {
    uint8_t value;
    uint8_t mask = 0x08;

    value = acc_lis2dhtr_read(device, LIS2DH_CTRL_REG1);
    if (state) {
        value |= mask;
    }
    else {
        value &= (~mask);
    }

    acc_lis2dhtr_write(device, LIS2DH_CTRL_REG1, value);
}

static void acc_lis2dhtr_set_xyz(acc_lis2dhtr_device_t * device,
                                 uint8_t data_rate,
                                 bool state) {
    uint8_t value;
    uint8_t mask = 0x07;
    global_data_rate = data_rate;
    value = acc_lis2dhtr_read(device, LIS2DH_CTRL_REG1);

    if (state) {
        value |= ((data_rate << 4) | mask);
    }
    else {
        value &= ((data_rate << 4) | (~mask));
    }
    acc_lis2dhtr_write(device, LIS2DH_CTRL_REG1, value);
}

static void acc_lis2dhtr_get_xyz(acc_lis2dhtr_device_t * device) {

    // spin_lock_saved_state_t lock_state;
    // spin_lock_irqsave(&acc.lock, lock_state);


    int16_t acc_x = (((int16_t)acc_lis2dhtr_read(device, LIS2DH_OUT_X_H) << 8) | acc_lis2dhtr_read(device, LIS2DH_OUT_X_L));
    int16_t acc_y = (((int16_t)acc_lis2dhtr_read(device, LIS2DH_OUT_Y_H) << 8) | acc_lis2dhtr_read(device, LIS2DH_OUT_Y_L));
    int16_t acc_z = (((int16_t)acc_lis2dhtr_read(device, LIS2DH_OUT_Z_H) << 8) | acc_lis2dhtr_read(device, LIS2DH_OUT_Z_L));

    // calibrate the accelometer readings
    /* by default the scale range of the accelerometer is set at +-2g
     the resolution of the sensor is 16bit but 1 bit is used as sign bit so the magnitude
     is coded on only 15 bit
     */

    device->axe.x = (float)acc_x*sensitivity/16;
    device->axe.y = (float)acc_y*sensitivity/16;
    device->axe.z = (float)acc_z*sensitivity/16;

    // spin_unlock_irqrestore(&acc.lock, lock_state);
}


/* function to set acceleromter range : +-2 , +-4 , +-8 , +-16 )*/

static void acc_lis2dhtr_set_scale(struct acc_lis2dhtr_device * device,int scale){

    u_int8_t reg = acc_lis2dhtr_read(device, LIS2DH_CTRL_REG4);
    global_scale = scale;

    switch (scale){

        case 2 :
        reg &=~ (1 << 4);
        reg &=~ (1 << 5);
        acc_lis2dhtr_write(device, LIS2DH_CTRL_REG4,reg);
        sensitivity = SCALE_SENSITIVITY_2G;
        break;

        case 4 :
        reg &=~ (1 << 5);
        reg |=  (1 << 4);
        acc_lis2dhtr_write(device, LIS2DH_CTRL_REG4,reg);
        sensitivity = SCALE_SENSITIVITY_4G;
        break;

        case 8 :
        reg &=~ (1 << 4);
        reg |=  (1 << 5);
        acc_lis2dhtr_write(device, LIS2DH_CTRL_REG4,reg);
        sensitivity = SCALE_SENSITIVITY_8G;
        break;

        case 16 :
        reg |=  (1 << 4);
        reg |=  (1 << 5);
        acc_lis2dhtr_write(device, LIS2DH_CTRL_REG4,reg);
        sensitivity = SCALE_SENSITIVITY_16G;
        break;

    }

}


/* function to disable interrupt on pin1 or pin2*/

static void acc_disable_interrupt(struct acc_lis2dhtr_device * device, int pin){

    if(pin == 1){

        // disable interrupt for pin 1
        uint8_t ctrl_reg3 = acc_lis2dhtr_read(device, LIS2DH_CTRL_REG3);
        // reset I1_IA1
        ctrl_reg3 &=~ (1 << 6);
        // reset I1_IA2
        ctrl_reg3 &=~ (1 << 5);
        acc_lis2dhtr_write(device, LIS2DH_CTRL_REG3,ctrl_reg3);

    }

    else if(pin == 2){

        // disable interrupt on pin 2
        uint8_t ctrl_reg6 = acc_lis2dhtr_read(device, LIS2DH_CTRL_REG6);
        // reset I2_IA1
        ctrl_reg6 &=~  (1 << 6);
        // reset I2_IA2
        ctrl_reg6 &=~  (1 << 5);
        acc_lis2dhtr_write(device, LIS2DH_CTRL_REG6,ctrl_reg6);
    }
    
}


/* function to get interrupt events and refresh the source register*/

static void acc_clear_configuration(struct acc_lis2dhtr_device * device){


    uint8_t  int1_src = acc_lis2dhtr_read(device, LIS2DH_INT1_SOURCE);

    // print interrupt events occured (or not) on pin 1
    
    if(int1_src & (1 << 6)) printf("interrupt active\n");
    if(int1_src & (1 << 5)) printf("Z active high\n");
    if(int1_src & (1 << 4)) printf("Z active low\n");
    if(int1_src & (1 << 3)) printf("Y active high\n");
    if(int1_src & (1 << 2)) printf("Y active low\n");
    if(int1_src & (1 << 1)) printf("X active high\n");
    if(int1_src & (1 << 0)) printf("X active low\n");

    // by reading this register all events are cleared
    
    printf("interrupt configuration 1 was cleared\n");

    uint8_t  int2_src = acc_lis2dhtr_read(device, LIS2DH_INT2_SOURCE);

    // print interrupt events occured (or not) on pin 2
    
    if(int2_src & (1 << 6)) printf("interrupt active\n");
    if(int2_src & (1 << 5)) printf("Z active high\n");
    if(int2_src & (1 << 4)) printf("Z active low\n");
    if(int2_src & (1 << 3)) printf("Y active high\n");
    if(int2_src & (1 << 2)) printf("Y active low\n");
    if(int2_src & (1 << 1)) printf("X active high\n");
    if(int2_src & (1 << 0)) printf("X active low\n");

    // by reading this register all events are cleared

    printf("interrupt configuration 2 was cleared\n");
    
 
}

/* function to configure the intrruptions on pin x 
the trheshold should be given in mg
the duration  should be given in ms
*/
static void acc_configure_interrupt(struct acc_lis2dhtr_device * device, int pin, int threshold, int duration){

    uint8_t threshold_reg = 0x00;

    // disable high-pass filter
    uint8_t reg2 = 0x00;
    uint8_t g_scale;
    acc_lis2dhtr_write(device, LIS2DH_CTRL_REG2,reg2);

    /* calculate threshold register*/

    /* set threshold : must be calculated from the range scale*/

    switch(global_scale){

        case 2:
        g_scale = (uint8_t) (threshold / 16 );
        threshold_reg=   g_scale & (0x7F);
        break;

        case 4:
        g_scale = (uint8_t) (threshold / 32 );
        threshold_reg=   g_scale & (0x7F);
        break;

        case 8:
        g_scale = (uint8_t) (threshold / 62 );
        threshold_reg=   g_scale & (0x7F);
        break;

        case 16:
        g_scale = (uint8_t) (threshold / 186 );
        threshold_reg=   g_scale & (0x7F);
        break;
        
    }

    /* calculate the event duration*/
    /* Configure duration: duration = N/ODR  (duration is given in ms)*/

    float   duration_s     = ((float)duration/1000) * global_data_rate;
    uint8_t duration_reg   = (uint8_t)(duration_s)& 0x7F;


    

    if(pin == 1){

        /* enabling interrupt on pin 1 by setting IA1 and IA2 */

        uint8_t ctrl_reg3 = acc_lis2dhtr_read(device, LIS2DH_CTRL_REG3);
        // set I1_IA1
        ctrl_reg3 |= (1 << 6);
        // set I1_IA2
        ctrl_reg3 |= (1 << 5);
        acc_lis2dhtr_write(device, LIS2DH_CTRL_REG3,ctrl_reg3);

        /* latch interrupt request on INT1_SRC*/
        uint8_t ctrl_reg5 = acc_lis2dhtr_read(device, LIS2DH_CTRL_REG5);
        ctrl_reg5 |= (1 << 3);
        acc_lis2dhtr_write(device, LIS2DH_CTRL_REG5,ctrl_reg5);

        /* set threshold register*/
        acc_lis2dhtr_write(device, LIS2DH_INT1_THS,threshold_reg);

        /* set event duration register*/
        acc_lis2dhtr_write(device, LIS2DH_INT1_DURATION,duration_reg);

        /* set interrupt on 6 direction X- X+ Y- Y+ Z- Z+ with OR condition*/
        // pin 1
        uint8_t int1_cfg =  0x2A;
        acc_lis2dhtr_write(device, LIS2DH_INT1_CFG,int1_cfg);

    }

    else if(pin == 2){

        /* enabling interrupt on pin 2 by setting IA1 and IA2 */

        uint8_t ctrl_reg6 = acc_lis2dhtr_read(device, LIS2DH_CTRL_REG6);
        // set I2_IA1
        ctrl_reg6 |= (1 << 6);
        // set I2_IA2
        ctrl_reg6 |= (1 << 5);
        
        acc_lis2dhtr_write(device, LIS2DH_CTRL_REG6,ctrl_reg6);

        /* latch interrupt request on INT2_SRC*/
        uint8_t ctrl_reg5 = acc_lis2dhtr_read(device, LIS2DH_CTRL_REG5);
        ctrl_reg5 |= (1 << 1);
        acc_lis2dhtr_write(device, LIS2DH_CTRL_REG5,ctrl_reg5);

        /* set threshold register*/
        acc_lis2dhtr_write(device, LIS2DH_INT2_THS,threshold_reg);

        /* set event duration register*/
        acc_lis2dhtr_write(device, LIS2DH_INT2_DURATION,duration_reg);

        /* set interrupt on 6 direction X- X+ Y- Y+ Z- Z+ with OR condition*/
        // pin 2

        uint8_t int2_cfg =  0x2A;
        acc_lis2dhtr_write(device, LIS2DH_INT_CFG,int2_cfg);


    }   

}




void acc_lis2dhtr_init(acc_lis2dhtr_device_t * device,
                       uint8_t bus,
                       uint16_t cs_pin)
{
    device->bus_id              = bus;
    device->nss_pin             = cs_pin;
    device->axe.x               = 0;
    device->axe.y               = 0;
    device->axe.z               = 0;
    device->get_id              = &acc_lis2dhtr_get_id;
    device->set_lowpower        = &acc_lis2dhtr_set_lowpower;
    device->set_xyz             = &acc_lis2dhtr_set_xyz;
    device->get_xyz             = &acc_lis2dhtr_get_xyz;
    device->set_scale           = &acc_lis2dhtr_set_scale;
    device->disable_interrupt   = &acc_disable_interrupt;
    device->clear_configuration = &acc_clear_configuration;
    device->configure_interrupt = &acc_configure_interrupt;

}

