#ifndef _DEV_ACCELERO_LIS2DHTR_H_
#define _DEV_ACCELERO_LIS2DHTR_H_

#define I_AM_LIS2DH             0x33

/* LIS2DH Registers */
#define LIS2DH_STATUS_REG_AUX   0x07
#define LIS2DH_OUT_TEMP_L       0x0C
#define LIS2DH_OUT_TEMP_H       0x0D
#define LIS2DH_INT_COUNTER_REG  0x0E
#define LIS2DH_WHO_AM_I         0x0F
#define LIS2DH_TEMP_CFG_REG     0x1F
#define LIS2DH_CTRL_REG1        0x20
#define LIS2DH_CTRL_REG2        0x21
#define LIS2DH_CTRL_REG3        0x22
#define LIS2DH_CTRL_REG4        0x23
#define LIS2DH_CTRL_REG5        0x24
#define LIS2DH_CTRL_REG6        0x25
#define LIS2DH_REFERENCE        0x26
#define LIS2DH_STATUS_REG2      0x27
#define LIS2DH_OUT_X_L          0x28
#define LIS2DH_OUT_X_H          0x29
#define LIS2DH_OUT_Y_L          0x2A
#define LIS2DH_OUT_Y_H          0x2B
#define LIS2DH_OUT_Z_L          0x2C
#define LIS2DH_OUT_Z_H          0x2D
#define LIS2DH_FIFO_CTRL_REG    0x2E
#define LIS2DH_FIFO_SRC_REG     0x2F
#define LIS2DH_INT1_CFG         0x30
#define LIS2DH_INT1_SOURCE      0x31
#define LIS2DH_INT1_THS         0x32
#define LIS2DH_INT1_DURATION    0x33
#define LIS2DH_INT_CFG          0x34
#define LIS2DH_INT2_SOURCE      0x35
#define LIS2DH_INT2_THS         0x36
#define LIS2DH_INT2_DURATION    0x37
#define LIS2DH_CLICK_CFG        0x38
#define LIS2DH_CLICK_SRC        0x39
#define LIS2DH_CLICK_THS        0x3A
#define LIS2DH_TIME_LIMIT       0x3B
#define LIS2DH_TIME_LATENCY     0x3C
#define LIS2DH_TIME_WINDOW      0x3D
#define LIS2DH_ACT_THS          0x3E
#define LIS2DH_ACT_DUR          0x3F

/* LIS2DH Data rate */
#define LIS2DH_DR_POWERDOWEN    0x00
#define LIS2DH_DR___1HZ         0x01
#define LIS2DH_DR__10HZ         0x02
#define LIS2DH_DR__25HZ         0x03
#define LIS2DH_DR__50HZ         0x04
#define LIS2DH_DR_100HZ         0x05
#define LIS2DH_DR_200HZ         0x06
#define LIS2DH_DR_400HZ         0x07
#define LIS2DH_DR_LP1_6HZ       0x08
#define LIS2DH_DR_LP4_3HZ       0x09

/* scale range sensibilties */

#define SCALE_SENSITIVITY_2G  0.001
#define SCALE_SENSITIVITY_4G  0.002
#define SCALE_SENSITIVITY_8G  0.004
#define SCALE_SENSITIVITY_16G 0.012

//global  int scale range 4;
typedef struct {;
    float     x;
    float     y;
    float     z;
} acc_axes_t;

typedef struct acc_lis2dhtr_device {
    uint8_t    bus_id;
    uint16_t   nss_pin;
    acc_axes_t axe;
    u_int8_t (*get_id)              (struct acc_lis2dhtr_device *);
    void     (*set_lowpower)        (struct acc_lis2dhtr_device *, bool);
    void     (*set_xyz)             (struct acc_lis2dhtr_device *, uint8_t, bool);
    void     (*get_xyz)             (struct acc_lis2dhtr_device *);
    void     (*set_scale)           (struct acc_lis2dhtr_device *, int);
    void     (*disable_interrupt)   (struct acc_lis2dhtr_device *, int);
    void     (*clear_configuration) (struct acc_lis2dhtr_device *);
    void     (*configure_interrupt) (struct acc_lis2dhtr_device *,int, int, int);
} acc_lis2dhtr_device_t;

void acc_lis2dhtr_init(acc_lis2dhtr_device_t * device,
                       uint8_t bus,
                       uint16_t cs_pin);

#endif  // _DEV_ACCELERO_LIS2DHTR_H_
