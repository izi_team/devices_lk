/*
 * Copyright (c) 2025 IZITRON
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */


/* General purpose registers */
#define QMI8658_WHO_AM_I    0x00  // WHO_AM_I register address.
#define QMI8658_REVISION    0x01  // REVISION register address.

/* Setup and control registers */
#define QMI8658_CTRL1       0x02  // Control register 1 address.
#define QMI8658_CTRL2       0x03  // Control register 2 address.
#define QMI8658_CTRL3       0x04  // Control register 3 address.
#define QMI8658_CTRL4       0x05  // Control register 4 address.
#define QMI8658_CTRL5       0x06  // Control register 5 address.
#define QMI8658_CTRL6       0x07  // Control register 6 address.
#define QMI8658_CTRL7       0x08  // Control register 7 address.
#define QMI8658_CTRL9       0x0A  // Control register 9 address.


/* Calibration registers */
#define QMI8658_CAL1_L      0x0B  // Calibation regiter 1 low byte.
#define QMI8658_CAL1_H      0x0C  // Calibation regiter 1 high byte.
#define QMI8658_CAL2_L      0x0D  // Calibation regiter 2 low byte.
#define QMI8658_CAL2_H      0x0E  // Calibation regiter 2 high byte.
#define QMI8658_CAL3_L      0x0F  // Calibation regiter 3 low byte.
#define QMI8658_CAL3_H      0x10  // Calibation regiter 3 high byte.
#define QMI8658_CAL4_L      0x11  // Calibation regiter 4 low byte.
#define QMI8658_CAL4_H      0x12  // Calibation regiter 4 high byte.

/* Status registers */
#define QMI8658_STATUSINT   0x2D  // Status register INT
#define QMI8658_STATUS0     0x2E  // Status register 0
#define QMI8658_STATUS1     0x2F  // Status register 1

/* Data output registers */

// Accelerometer
#define QMI8658_ACC_X_L     0x35  // Accelerometer X-axis low byte.
#define QMI8658_ACC_X_H     0x36  // Accelerometer X-axis high byte.
#define QMI8658_ACC_Y_L     0x37  // Accelerometer Y-axis low byte.
#define QMI8658_ACC_Y_H     0x38  // Accelerometer Y-axis high byte.
#define QMI8658_ACC_Z_L     0x39  // Accelerometer Z-axis low byte.
#define QMI8658_ACC_Z_H     0x3A  // Accelerometer Z-axis high byte.

// Gyroscope
#define QMI8658_GYR_X_L     0x3B  // Gyroscope X-axis low byte.
#define QMI8658_GYR_X_H     0x3C  // Gyroscope X-axis high byte.
#define QMI8658_GYR_Y_L     0x3D  // Gyroscope Y-axis low byte.
#define QMI8658_GYR_Y_H     0x3E  // Gyroscope Y-axis high byte.
#define QMI8658_GYR_Z_L     0x3F  // Gyroscope Z-axis low byte.
#define QMI8658_GYR_Z_H     0x40  // Gyroscope Z-axis high byte.

// Temperature sensor
#define QMI8658_TEMP_L      0x33  // Temperature sensor low byte.
#define QMI8658_TEMP_H      0x34  // Temperature sensor high byte.

/* Soft reset register */
#define QMI8658_RESET       0x60  // Soft reset register address.

/* Enum representing the output data rate (ODR) settings for the accelerometer */
typedef enum {
    acc_odr_8000,      // Accelerometer ODR set to 8000 Hz.
    acc_odr_4000,      // Accelerometer ODR set to 4000 Hz.
    acc_odr_2000,      // Accelerometer ODR set to 2000 Hz.
    acc_odr_1000,      // Accelerometer ODR set to 1000 Hz.
    acc_odr_500,       // Accelerometer ODR set to 500 Hz.
    acc_odr_250,       // Accelerometer ODR set to 250 Hz.
    acc_odr_125,       // Accelerometer ODR set to 125 Hz.
    acc_odr_62_5,      // Accelerometer ODR set to 62.5 Hz.
    acc_odr_31_25,     // Accelerometer ODR set to 31.25 Hz.
    acc_odr_128 = 12,  // Accelerometer ODR set to 128 Hz.
    acc_odr_21,        // Accelerometer ODR set to 21 Hz.
    acc_odr_11,        // Accelerometer ODR set to 11 Hz.
    acc_odr_3,         // Accelerometer ODR set to 3 Hz.
} acc_odr_t;

/* Enum representing the output data rate (ODR) settings for the gyroscope */
typedef enum {
    gyro_odr_8000,     // Gyroscope ODR set to 8000 Hz.
    gyro_odr_4000,     // Gyroscope ODR set to 4000 Hz.
    gyro_odr_2000,     // Gyroscope ODR set to 2000 Hz.
    gyro_odr_1000,     // Gyroscope ODR set to 1000 Hz.
    gyro_odr_500,      // Gyroscope ODR set to 500 Hz.
    gyro_odr_250,      // Gyroscope ODR set to 250 Hz.
    gyro_odr_125,      // Gyroscope ODR set to 125 Hz.
    gyro_odr_62_5,     // Gyroscope ODR set to 62.5 Hz.
    gyro_odr_31_25,    // Gyroscope ODR set to 31.25 Hz.
} gyro_odr_t;
/* Enum representing the scale settings for the accelerometer */
typedef enum {
    acc_scale_2g = 0,    // Accelerometer scale set to ±2g.
    acc_scale_4g,        // Accelerometer scale set to ±4g.
    acc_scale_8g,        // Accelerometer scale set to ±8g.
    acc_scale_16g,       // Accelerometer scale set to ±16g.
} acc_scale_t;

/* Enum representing the scale settings for the gyroscope */
typedef enum {
    gyro_scale_16dps = 0,       // Gyroscope scale set to ±16 degrees per second.
    gyro_scale_32dps,            // Gyroscope scale set to ±32 degrees per second.
    gyro_scale_64dps,            // Gyroscope scale set to ±64 degrees per second.
    gyro_scale_128dps,           // Gyroscope scale set to ±128 degrees per second.
    gyro_scale_256dps,           // Gyroscope scale set to ±256 degrees per second.
    gyro_scale_512dps,           // Gyroscope scale set to ±512 degrees per second.
    gyro_scale_1024dps,          // Gyroscope scale set to ±1024 degrees per second.
    gyro_scale_2048dps,          // Gyroscope scale set to ±2048 degrees per second.
} gyro_scale_t;

/* define scale sensitivity */
/* Accelerometer scale sensitivity values for different gravity ranges */
#define ACC_SCALE_SENSITIVITY_2G        (1 << 14)  // Sensitivity for ±2g range.
#define ACC_SCALE_SENSITIVITY_4G        (1 << 13)  // Sensitivity for ±4g range.
#define ACC_SCALE_SENSITIVITY_8G        (1 << 12)  // Sensitivity for ±8g range.
#define ACC_SCALE_SENSITIVITY_16G       (1 << 11)  // Sensitivity for ±16g range.

/* Gyroscope scale sensitivity values for different degrees per second ranges */
#define GYRO_SCALE_SENSITIVITY_16DPS    (1 << 11)  // Sensitivity for ±16 degrees per second range.
#define GYRO_SCALE_SENSITIVITY_32DPS    (1 << 10)  // Sensitivity for ±32 degrees per second range.
#define GYRO_SCALE_SENSITIVITY_64DPS    (1 << 9 )  // Sensitivity for ±64 degrees per second range.
#define GYRO_SCALE_SENSITIVITY_128DPS   (1 << 8 )  // Sensitivity for ±128 degrees per second range.
#define GYRO_SCALE_SENSITIVITY_256DPS   (1 << 7 )  // Sensitivity for ±256 degrees per second range.
#define GYRO_SCALE_SENSITIVITY_512DPS   (1 << 6 )  // Sensitivity for ±512 degrees per second range.
#define GYRO_SCALE_SENSITIVITY_1024DPS  (1 << 5 )  // Sensitivity for ±1024 degrees per second range.
#define GYRO_SCALE_SENSITIVITY_2048DPS  (1 << 4 )  // Sensitivity for ±2048 degrees per second range.

/* Temperature sensor resolution */
#define TEMPERATURE_SENSOR_RESOLUTION   (1 << 8 )  // Temperature sensor resolution (ADC)

/* define angular acceleration unit 
    uncomment the desired unit definition
    DPS : degree per second 
    RPS : radian per second
*/

//#define GYRO_UNIT_DPS       1
#define GYRO_UNIT_RPS       1
#define DPS_TO_RPS_RATIO    0.017453


#define ONE_G                       (0.9807f)
#define ACC_X_POSITVE_DEVIATION     (0.00f  )
#define ACC_X_NEGATIVE_DEVIATION    (0.005  )
#define ACC_Y_POSITVE_DEVIATION     (0.13f  )
#define ACC_Y_NEGATIVE_DEVIATION    (0.17f  )
#define ACC_Z_POSITVE_DEVIATION     (0.05f  )
#define ACC_Z_NEGATIVE_DEVIATION    (0.00f  )

#define CALIBRATEX(x)        ((x>0) ? (x-ACC_X_POSITVE_DEVIATION):(x-ACC_X_NEGATIVE_DEVIATION))
#define CALIBRATEY(x)        ((x>0) ? (x-ACC_Y_POSITVE_DEVIATION):(x-ACC_Y_NEGATIVE_DEVIATION))
#define CALIBRATEZ(x)        ((x>0) ? (x+ACC_Z_POSITVE_DEVIATION):(x))

/* Struct representing the axes data for accelerometer */
typedef struct {
    float x;    // Accelerometer data along the x-axis.
    float y;    // Accelerometer data along the y-axis.
    float z;    // Accelerometer data along the z-axis.
} acc_axes_t;

/* Struct representing the axes data for gyroscope */
typedef struct {
    float x;    // Gyroscope data along the x-axis.
    float y;    // Gyroscope data along the y-axis.
    float z;    // Gyroscope data along the z-axis.
} gyro_axes_t;

/* Struct representing the data read from Qmi8658c */
typedef struct {
    acc_axes_t  acc_xyz;       // Accelerometer data in three axes (x, y, z).
    gyro_axes_t gyro_xyz;      // Gyroscope data in three axes (x, y, z).
    float temperature;         // Temperature reading from Qmi8658c.
} qmi_data_t;

/* Enum representing the mode of operation for Qmi8658c */
typedef enum {
    qmi8658_mode_acc_only = 1,   // Mode for accelerometer-only operation.
    qmi8658_mode_gyro_only,      // Mode for gyroscope-only operation.
    qmi8658_mode_dual,           // Mode for dual accelerometer and gyroscope operation.
} qmi8658_mode_t;

/* Qmi8658 config */

/* Struct representing the configuration settings for Qmi8658c */
typedef struct {
    qmi8658_mode_t qmi8658_mode;    // Mode of operation for Qmi8658c.
    acc_scale_t acc_scale;          // Scale setting for the accelerometer.
    acc_odr_t acc_odr;              // Output data rate (ODR) setting for the accelerometer.
    gyro_scale_t gyro_scale;        // Scale setting for the gyroscope.
    gyro_odr_t gyro_odr;            // Output data rate (ODR) setting for the gyroscope.
} qmi8658_cfg_t;

/* Enum representing the result of an operation with Qmi8658c */
typedef enum {
    qmi8658_result_init_success,   // Operation to init Qmi8658c was successful.
    qmi8658_result_init_error,     // Operation to init Qmi8658c failed.
    qmi8658_result_open_success,   // Operation to open communication with Qmi8658c was successful.
    qmi8658_result_open_error,     // Error occurred while trying to open communication with Qmi8658c.
    qmi8658_result_close_success,  // Operation to close communication with Qmi8658c was successful.
    qmi8658_result_close_error,    // Error occurred while trying to close communication with Qmi8658c.
} qmi8658_result_t;


typedef struct qmi8658_device {
    uint8_t             bus_id;
    uint16_t            cs_pin;
    qmi8658_result_t    (*init)(struct qmi8658_device *);
    qmi8658_result_t    (*open)(struct qmi8658_device *, qmi8658_cfg_t*);
    void                (*read)(struct qmi8658_device *, qmi_data_t*);
    qmi8658_result_t    (*close)(struct qmi8658_device *);
    void                (*acc_set_scale)(struct qmi8658_device *, acc_scale_t);
    void                (*acc_set_odr)(struct qmi8658_device *, acc_odr_t);
    void                (*gyro_set_scale)(struct qmi8658_device *, gyro_scale_t);
    void                (*gyro_set_odr)(struct qmi8658_device *, gyro_odr_t);
    void                (*power_down)(struct qmi8658_device *);
    void                (*power_on)(struct qmi8658_device *);
    uint8_t             (*get_id)(struct qmi8658_device *);
} qmi8658_device_t;

void qmi8658_init(qmi8658_device_t * device,
                       uint8_t bus,
                       uint16_t cs_pin);


const char* qmi8658_result_to_str(qmi8658_result_t result);