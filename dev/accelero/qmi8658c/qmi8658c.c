/*
 * Copyright (c) 2025 IZITRON
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <stdlib.h>
#include <app.h>
#include <string.h>
#include <platform/spi.h>
#include <lk/trace.h>
#include <hardware/gpio.h>
#include <kernel/thread.h>
#include "qmi8658c.h"

#define LOCAL_TRACE 0

/* Structure for QMI context */
typedef struct {
    uint16_t acc_sensitivity;       // Sensitivity value for the accelerometer.
    uint8_t acc_scale;              // Scale setting for the accelerometer.
    uint16_t gyro_sensitivity;      // Sensitivity value for the gyroscope.
    uint8_t gyro_scale;             // Scale setting for the gyroscope.
    qmi8658_mode_t qmi8658_mode;    // Sensor mode
    bool imu_opened;                // True if the IMU is opened
} qmi_ctx_t;

qmi_ctx_t qmi_ctx;  // QMI context instance.

/* Accelerometer sensitivity table */
uint16_t acc_scale_sensitivity_table[4] = {
    ACC_SCALE_SENSITIVITY_2G,   // Sensitivity for ±2g range.
    ACC_SCALE_SENSITIVITY_4G,   // Sensitivity for ±4g range.
    ACC_SCALE_SENSITIVITY_8G,   // Sensitivity for ±8g range.
    ACC_SCALE_SENSITIVITY_16G   // Sensitivity for ±16g range.
};
                                  
/* Gyroscope sensitivity table */
uint16_t gyro_scale_sensitivity_table[8] = {
    GYRO_SCALE_SENSITIVITY_16DPS,     // Sensitivity for ±16 degrees per second range.
    GYRO_SCALE_SENSITIVITY_32DPS,     // Sensitivity for ±32 degrees per second range.
    GYRO_SCALE_SENSITIVITY_64DPS,     // Sensitivity for ±64 degrees per second range.
    GYRO_SCALE_SENSITIVITY_128DPS,    // Sensitivity for ±128 degrees per second range.
    GYRO_SCALE_SENSITIVITY_256DPS,    // Sensitivity for ±256 degrees per second range.
    GYRO_SCALE_SENSITIVITY_512DPS,    // Sensitivity for ±512 degrees per second range.
    GYRO_SCALE_SENSITIVITY_1024DPS,   // Sensitivity for ±1024 degrees per second range.
    GYRO_SCALE_SENSITIVITY_2048DPS    // Sensitivity for ±2048 degrees per second range.
};


/*################################# Functions for spi communication #################################*/

static void qmi8658_write(qmi8658_device_t * device,
                               uint8_t reg,
                               uint8_t value) {
    int ret;
    uint8_t send_data[2];

    send_data[0] = reg & 0x7f;
    send_data[1] = value;

    gpio_put(device->cs_pin, 0);
    ret=spi_transmit(device->bus_id, send_data, 2);
    if (ret) TRACEF("Write to QMI fail: %d\n", ret);
    gpio_put(device->cs_pin, 1);

    // give the sensor time to process
    thread_sleep(1);
}

static uint8_t qmi8658_read(qmi8658_device_t * device,
                              uint8_t reg) {
    int ret;
    uint8_t value = 0;
    uint8_t addr;

    addr = reg | 0x80;

    gpio_put(device->cs_pin, 0);
    ret = spi_receive(device->bus_id,addr, &value, 1);
    if (ret) TRACEF("Read from QMI fail: %d\n", ret);
    gpio_put(device->cs_pin, 1);

    // give the sensor time to process
    thread_sleep(1);

    return value;
}


/*####################################### Functions for accelerometr #########################################*/

// Set output data rate of the accelerometer
static void acc_set_odr(qmi8658_device_t * device, acc_odr_t odr) {

    uint8_t qmi8658_ctrl2_reg = qmi8658_read(device, QMI8658_CTRL2);
    
    qmi8658_ctrl2_reg = (0xF0 & qmi8658_ctrl2_reg) | odr;
    qmi8658_write(device, QMI8658_CTRL2, qmi8658_ctrl2_reg);
}

// Set the scale of the accelerometer
static void acc_set_scale(qmi8658_device_t * device, acc_scale_t acc_scale) {

    uint8_t qmi8658_ctrl2_reg = qmi8658_read(device, QMI8658_CTRL2);
    qmi_ctx.acc_scale = acc_scale;
    qmi_ctx.acc_sensitivity = acc_scale_sensitivity_table[acc_scale];
    
    qmi8658_ctrl2_reg = (0x8F & qmi8658_ctrl2_reg) | (acc_scale << 4);
    qmi8658_write(device, QMI8658_CTRL2,qmi8658_ctrl2_reg);
}

/*######################################## Functions for gyroscope ##########################################*/

// Set Gyroscope Output Data Rate (ODR)
static void gyro_set_odr(qmi8658_device_t * device, gyro_odr_t odr){

    uint8_t qmi8658_ctrl3_reg = qmi8658_read(device, QMI8658_CTRL3);
    
    qmi8658_ctrl3_reg = (0xF0 & qmi8658_ctrl3_reg) | odr;
    qmi8658_write(device, QMI8658_CTRL3,qmi8658_ctrl3_reg); 
}

// Set the scale of the Gyroscope
static void gyro_set_scale(qmi8658_device_t * device, gyro_scale_t gyro_scale) {

    uint8_t qmi8658_ctrl3_reg = qmi8658_read(device, QMI8658_CTRL3);
    qmi_ctx.gyro_scale = gyro_scale;
    qmi_ctx.gyro_sensitivity = gyro_scale_sensitivity_table[gyro_scale];
    
    qmi8658_ctrl3_reg = (0x8F & qmi8658_ctrl3_reg) | (gyro_scale << 4);
    qmi8658_write(device, QMI8658_CTRL3,qmi8658_ctrl3_reg);
}

/*######################################### General functions for qmi configuration ##########################*/

// Reset all regesters of the qmi8658 : 
// To be done before starting interfacing with the sensor to make sure it's on a "konwn" state

static void qmi_reset(qmi8658_device_t *device){
  
    qmi8658_write(device, QMI8658_RESET,0xB0);
}

// Get device ID
static uint8_t qmi8658_get_id(qmi8658_device_t *device) {
   
    return  qmi8658_read(device, QMI8658_WHO_AM_I);
    
}

static qmi8658_result_t init(qmi8658_device_t *device) {
    uint8_t device_id;

    // clear context
    memset(&qmi_ctx, 0, sizeof(qmi_ctx_t));

    device_id = qmi8658_get_id(device);

    if (!device_id) {
        // dvice ID is not valid, device communication failed
        return qmi8658_result_init_error;
    }

    return qmi8658_result_init_success;
}

static void select_mode(qmi8658_device_t *device, qmi8658_mode_t qmi8658_mode ) {
  
    uint8_t qmi8658_ctrl7_reg = qmi8658_read(device, QMI8658_CTRL7);
    
    qmi8658_ctrl7_reg = (0xFC & qmi8658_ctrl7_reg) | qmi8658_mode;
    qmi8658_write(device, QMI8658_CTRL7,qmi8658_ctrl7_reg);   
}

/*######################################### class functions #################################################*/

// Open communication with the QMI8658 sensor and initializes it with the provided configuration settings.
// Return a status code indicating success or failure of the operation.

static qmi8658_result_t open(qmi8658_device_t *device, qmi8658_cfg_t* qmi8658_cfg) {

    qmi8658_result_t result;
    uint8_t qmi8658_ctrl7;

    // No valied config
    if (!qmi8658_cfg) {
        return qmi8658_result_open_error;
    }

    // IMU is already opened
    if (qmi_ctx.imu_opened) {
        return qmi8658_result_open_success;
    }

    qmi_ctx.qmi8658_mode = qmi8658_cfg->qmi8658_mode;
    
    // reset the sensor 
    qmi_reset(device);

    // set accelerometr and gyroscope scale and ODR   
    acc_set_odr(device, qmi8658_cfg->acc_odr);
    acc_set_scale(device, qmi8658_cfg->acc_scale);
    gyro_set_odr(device, qmi8658_cfg->gyro_odr);
    gyro_set_scale(device, qmi8658_cfg->gyro_scale);

    // Set sensor mode
    select_mode(device, qmi8658_cfg->qmi8658_mode);

    qmi8658_ctrl7 = qmi8658_read(device, QMI8658_CTRL7);
    result = (qmi8658_ctrl7 & 0x03) == qmi8658_cfg->qmi8658_mode ? qmi8658_result_open_success : qmi8658_result_open_error;
    
    if (result == qmi8658_result_open_success) {
        qmi_ctx.imu_opened = true;
    }

    return result;
}

// Read data from the QMI8658 sensor and stores it in the provided data structure.

static void read(qmi8658_device_t *device, qmi_data_t* data) {

    // Check if the sensor is opened
    if (!qmi_ctx.imu_opened) {
        return;
    }

    // read accelerometer data
    int16_t acc_x = (((int16_t)qmi8658_read(device, QMI8658_ACC_X_H) << 8) | qmi8658_read(device, QMI8658_ACC_X_L));
    int16_t acc_y = (((int16_t)qmi8658_read(device, QMI8658_ACC_Y_H) << 8) | qmi8658_read(device, QMI8658_ACC_Y_L));
    int16_t acc_z = (((int16_t)qmi8658_read(device, QMI8658_ACC_Z_H) << 8) | qmi8658_read(device, QMI8658_ACC_Z_L));
    data->acc_xyz.x = CALIBRATEX((float)acc_x/qmi_ctx.acc_sensitivity);
    data->acc_xyz.y = CALIBRATEX((float)acc_y/qmi_ctx.acc_sensitivity);
    data->acc_xyz.z = CALIBRATEX((float)acc_z/qmi_ctx.acc_sensitivity);

    // read gyroscope data
    int16_t rot_x = (((int16_t)qmi8658_read(device, QMI8658_GYR_X_H) << 8) | qmi8658_read(device, QMI8658_GYR_X_L));
    int16_t rot_y = (((int16_t)qmi8658_read(device, QMI8658_GYR_Y_H) << 8) | qmi8658_read(device, QMI8658_GYR_Y_L));
    int16_t rot_z = (((int16_t)qmi8658_read(device, QMI8658_GYR_Z_H) << 8) | qmi8658_read(device, QMI8658_GYR_Z_L));
    
    // Convert dps to rps if defined by user
    #if defined(GYRO_UNIT_RPS)
    float dps_to_rps_ratio = DPS_TO_RPS_RATIO;
    #else 
    float dps_to_rps_ratio = 1;
    #endif

    data->gyro_xyz.x = (float)rot_x*dps_to_rps_ratio/qmi_ctx.gyro_sensitivity;
    data->gyro_xyz.y = (float)rot_y*dps_to_rps_ratio/qmi_ctx.gyro_sensitivity;
    data->gyro_xyz.z = (float)rot_z*dps_to_rps_ratio/qmi_ctx.gyro_sensitivity;

    // read temperature data
    int16_t temp = (((int16_t)qmi8658_read(device, QMI8658_TEMP_H) << 8) | qmi8658_read(device, QMI8658_TEMP_L));
    data->temperature = (float)temp/TEMPERATURE_SENSOR_RESOLUTION;
}

static qmi8658_result_t close(qmi8658_device_t *device) {
    uint8_t qmi8658_ctrl7_reg, qmi8658_ctrl1_reg;
    qmi8658_result_t qmi8658_result;
  
    qmi8658_ctrl7_reg = qmi8658_read(device, QMI8658_CTRL7);
  
    // disable accelerometer, gyroscope, magnetometer and attitude engine
    qmi8658_ctrl7_reg &= 0xF0; 
    qmi8658_write(device, QMI8658_CTRL7,qmi8658_ctrl7_reg);
  
    // disable sensor by turning off the internal 2 MHz oscillator 
    qmi8658_ctrl1_reg = qmi8658_read(device, QMI8658_CTRL1);
    qmi8658_ctrl1_reg |= (1 << 0); 
    qmi8658_write(device, QMI8658_CTRL1,qmi8658_ctrl1_reg);

    // read these two registers
    qmi8658_ctrl7_reg = qmi8658_read(device, QMI8658_CTRL7);
    qmi8658_ctrl1_reg = qmi8658_read(device, QMI8658_CTRL1);

    qmi8658_result = (!(qmi8658_ctrl7_reg & 0x0F) && (qmi8658_ctrl1_reg & 0x01)) ? qmi8658_result_close_success : qmi8658_result_close_error;
    
    if (qmi8658_result == qmi8658_result_close_success) {
        qmi_ctx.imu_opened = false;
    }
    
    return qmi8658_result;
}

// function to put the qmi in power down mode
/* Power down mode : all qmi functional blocks are switched off
   Digital interfaces remain on allowing communication with the host device
   All configuration registers and data contents are preserved
   The current in this mode is typically 20 uA
*/

static void qmi8658_power_down(qmi8658_device_t * device){

    uint8_t qmi8658_ctrl7_reg, qmi8658_ctrl1_reg;
    
    qmi8658_ctrl7_reg = qmi8658_read(device, QMI8658_CTRL7);

    // diable accelerometer, gyroscope attitude engine and gyroscope snooze mode
    qmi8658_ctrl7_reg &=~ (1 << 0);
    qmi8658_ctrl7_reg &=~ (1 << 1);
    qmi8658_ctrl7_reg &=~ (1 << 3);
    qmi8658_ctrl7_reg &=~ (1 << 4);
    qmi8658_write(device, QMI8658_CTRL7, qmi8658_ctrl7_reg);

    // disable sensor by turning off the internal 2 MHz oscillator 
    qmi8658_ctrl1_reg = qmi8658_read(device,QMI8658_CTRL1);
    qmi8658_ctrl1_reg |= (1 << 0); 
    qmi8658_write(device,QMI8658_CTRL1, qmi8658_ctrl1_reg);
}

static void qmi8658_power_on(qmi8658_device_t * device){

    // Exit the power down mode by enabling the internal 2 MHz oscillator 
    uint8_t qmi8658_ctrl1_reg = qmi8658_read(device, QMI8658_CTRL1);
    qmi8658_ctrl1_reg &=~ (1 << 0); 
    qmi8658_write(device,QMI8658_CTRL1, qmi8658_ctrl1_reg);

    // Set sensor mode
    select_mode(device, qmi_ctx.qmi8658_mode);
}


void qmi8658_init(qmi8658_device_t * device,
                       uint8_t bus,
                       uint16_t cs_pin)
{
    device->bus_id              = bus;
    device->cs_pin              = cs_pin;
    device->init                = &init;
    device->open                = &open;
    device->read                = &read;
    device->close               = &close;
    device->acc_set_scale       = &acc_set_scale;
    device->acc_set_odr         = &acc_set_odr;
    device->gyro_set_scale      = &gyro_set_scale;
    device->gyro_set_odr        = &gyro_set_odr;
    device->power_down          = &qmi8658_power_down;
    device->power_on            = &qmi8658_power_on;
    device->get_id              = &qmi8658_get_id;
}

// Convert a qmi8658_result_t enum value into a corresponding string.
const char* qmi8658_result_to_str(qmi8658_result_t result) {
   switch(result){
    case qmi8658_result_open_success :
      return "open-success";
    case qmi8658_result_open_error :
      return "open-error";
    case qmi8658_result_close_success :
      return "close-success";
    case qmi8658_result_close_error:
      return "close-error";
  }
  return "unknown-error"; // make the compiler happy!
}
