/*
 * Copyright (c) 2022 Mihail CHERCIU
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <stdlib.h>
#include <app.h>
#include <string.h>
#include <dev/i2c.h>
#include <dev/gpio.h>
#include <lk/trace.h>
#include <kernel/thread.h>
#include <platform/spi.h>
#include "arducam_mini5MP.h"
#include "ov5642.h"

#define LOCAL_TRACE 1

#define CAM_NR_RES          9

#define CAM_RES_320x240     0
#define CAM_RES_640x480     1
#define CAM_RES_1024x768    2
#define CAM_RES_1280x960    3
#define CAM_RES_1600x1200   4
#define CAM_RES_2048x1536   5
#define CAM_RES_2592x1944   6
#define CAM_RES_1920x1080   7
const char *cam5mp_res[CAM_NR_RES] = {
    "320x240",
    "640x480",
    "1024x768",
    "1280x960",
    "1600x1200",
    "2048x1536",
    "2592x1944",
    "1920x1080",
    "RES_ERROR"
};

static void spi_cam_write(arducam_mini5mp_device_t *device,
                          uint8_t address,
                          uint8_t data) {
    int ret;
    uint8_t buf[2];

    buf[0] = address | ARDUCAM_WRITE_BIT;
    buf[1] = data;

    gpio_set(device->spi_nss_pin, 0);

    /* Write */
    ret = spi_transmit(device->spi_bus_id, buf, 2);
    if (ret) TRACEF("Error to write over spi, code: %d\n", ret);

    gpio_set(device->spi_nss_pin, 1);
}

static uint8_t spi_cam_read(arducam_mini5mp_device_t *device,
                            uint8_t address) {
    int ret;
    uint8_t value = 0;
    uint8_t addr;

    addr = address & 0x7f;

    gpio_set(device->spi_nss_pin, 0);

    /* Write */
    ret = spi_transmit(device->spi_bus_id, &addr, 1);
    if (ret) TRACEF("Error to write over spi, code: %d\n", ret);

    /* Read */
    ret = spi_receive(device->spi_bus_id, &value, 1);
    if (ret) TRACEF("Error to read over spi, code: %d\n", ret);

    gpio_set(device->spi_nss_pin, 1);

    return value;
}

static void i2c_cam_read_8(arducam_mini5mp_device_t *device,
                           uint8_t writeReg,
                           uint8_t *readReg) {
    int ret;

    ret = i2c_transmit(device->i2c_bus_id, device->i2c_address, &writeReg, 1);
    if (ret) TRACEF("Error to write over i2c, code: %d\n", ret);
    ret = i2c_receive(device->i2c_bus_id, device->i2c_address, readReg, 1);
    if (ret) TRACEF("Error to read over i2c, code: %d\n", ret);
}

static void i2c_cam_read_16(arducam_mini5mp_device_t *device,
                            uint16_t writeReg,
                            uint8_t *readReg) {
    int ret;

    uint8_t buf[2];

    buf[0] = (uint8_t) (writeReg >> 8);
    buf[1] = (uint8_t) (writeReg & 0x00FF);

    ret = i2c_transmit(device->i2c_bus_id, device->i2c_address, buf, 2);
    if (ret) TRACEF("Error to write over i2c, code: %d\n", ret);
    ret = i2c_receive(device->i2c_bus_id, device->i2c_address, readReg, 1);
    if (ret) TRACEF("Error to read over i2c, code: %d\n", ret);
}

static void i2c_cam_write_8(arducam_mini5mp_device_t *device,
                            uint8_t writeReg,
                            uint8_t writeData) {
    int ret;
    uint8_t buf[2];

    buf[0] = writeReg;
    buf[1] = writeData;

    ret = i2c_transmit(device->i2c_bus_id, device->i2c_address, buf, 2);
    if (ret) TRACEF("Error to write over i2c, code: %d\n", ret);
}

static void i2c_cam_write_16(arducam_mini5mp_device_t *device,
                             uint16_t writeReg,
                             uint16_t writeData) {
    int ret;
    uint8_t buf[3];

    buf[0] = (uint8_t) (writeReg >> 8);
    buf[1] = (uint8_t) (writeReg & 0x00FF);
    buf[2] = (uint8_t) (writeData & 0x00FF);

    ret = i2c_transmit(device->i2c_bus_id, device->i2c_address, buf, 3);
    if (ret) TRACEF("Error to write over i2c, code: %d\n", ret);
}

static void i2c_cam_buf_8(arducam_mini5mp_device_t *device,
                          const struct sensor_reg reglist[]) {

    uint8_t reg_addr = 0;
    uint8_t reg_val = 0;
    const struct sensor_reg *next = reglist;

    while ((reg_addr != 0xff) | (reg_val != 0xff))
    {
        reg_addr =next->reg;
        reg_val = next->val;
        i2c_cam_write_8(device, reg_addr, reg_val);
        next++;
    }
}

static void i2c_cam_buf_16(arducam_mini5mp_device_t *device,
                           const struct sensor_reg reglist[]) {

    uint16_t reg_addr = 0;
    uint16_t reg_val = 0;
    const struct sensor_reg *next = reglist;

    while ((reg_addr != 0xffff) | (reg_val != 0xff))
    {
        reg_addr = next->reg;
        reg_val = next->val;
        //TRACEF("reg_addr: %x , reg_val: %x\n", reg_addr, reg_val);
        i2c_cam_write_16(device, reg_addr, reg_val);
        next++;
    }
}

void arducam_setJpegSize_5mp(arducam_mini5mp_device_t * device,
                         uint8_t size) {
    switch(size)
    {
        case CAM_RES_320x240:
            i2c_cam_buf_16(device, ov5642_320x240);
            break;
        case CAM_RES_640x480:
            i2c_cam_buf_16(device, ov5642_640x480);
            break;
        case CAM_RES_1024x768:
            i2c_cam_buf_16(device, ov5642_1024x768);
            break;
        case CAM_RES_1280x960:
            i2c_cam_buf_16(device, ov5642_1280x960);
            break;
        case CAM_RES_1600x1200:
            i2c_cam_buf_16(device, ov5642_1600x1200);
            break;
        case CAM_RES_2048x1536:
            i2c_cam_buf_16(device, ov5642_2048x1536);
            break;
        case CAM_RES_2592x1944:
            i2c_cam_buf_16(device, ov5642_2592x1944);
            break;
        default:
            i2c_cam_buf_16(device, ov5642_640x480);
            break;
    }
}

/* --- work with bit ---*/
static void set_bit(arducam_mini5mp_device_t * device, uint8_t addr, uint8_t val) {
    uint8_t tmp;

    tmp = spi_cam_read(device, addr);
    tmp |= val;
    spi_cam_write(device, addr, tmp);
}

static void clear_bit(arducam_mini5mp_device_t * device, uint8_t addr, uint8_t val) {
    uint8_t tmp;

    tmp = spi_cam_read(device, addr);
    tmp &= (~val);
    spi_cam_write(device, addr, tmp);
}

static uint8_t get_bit(arducam_mini5mp_device_t * device, uint8_t addr, uint8_t mask) {
    uint8_t tmp;

    tmp = spi_cam_read(device, addr);
    tmp &= mask;

    return tmp;
}
/*----------------------*/

/*-- work with buffer --*/
static void arducam_flush_fifo(arducam_mini5mp_device_t * device)
{
    spi_cam_write(device, ARDUCAM_FIFO, ARDUCAM_FIFO_CLEAR_MASK);
}

static int arducam_fifo_length(arducam_mini5mp_device_t * device)
{
    uint8_t fifo1,fifo2,fifo3;
    uint32_t len=0;

    fifo1 = spi_cam_read(device, ARDUCAM_FIFO_SIZE1);
    fifo2 = spi_cam_read(device, ARDUCAM_FIFO_SIZE2);
    fifo3 = spi_cam_read(device, ARDUCAM_FIFO_SIZE3) & 0x7f;
    len = (((uint32_t)fifo3 << 16) | ((uint32_t)fifo2 << 8) | (uint32_t)fifo1) & 0x07fffff;
    return len;
}

static void arducam_fifo_brust(arducam_mini5mp_device_t * device) {
    int ret;
    uint8_t tmp_read;
    uint8_t tmp_write;

    /* spi cam select */
    gpio_set(device->spi_nss_pin, 0);

    /* get first dummy byte, only for OV5642 */
    tmp_write = ARDUCAM_BURST_FIFO_READ;
    ret = spi_transmit(device->spi_bus_id, &tmp_write, 1);
    if (ret) TRACEF("Error to write over spi, code: %d\n", ret);

    ret = spi_receive(device->spi_bus_id, &tmp_read, 1);
    if (ret) TRACEF("Error to read over spi, code: %d\n", ret);

    gpio_set(device->spi_nss_pin, 1);
}

static void arducam_start_capture(arducam_mini5mp_device_t * device)
{
    spi_cam_write(device, ARDUCAM_FIFO, ARDUCAM_FIFO_START_MASK);
}

static void sleep_mode(arducam_mini5mp_device_t * device, int mode){

    int ret;
    // enter standby mode
    if(mode == ARDUCAM_STANDBY_MODE){
        
        set_bit(device,ARDUCAM_GPIO,ARDUCAM_GPIO_PWDN_MASK);
        thread_sleep(10);
        ret=get_bit(device,ARDUCAM_GPIO,ARDUCAM_GPIO_PWDN_MASK);
        if(ret==2)
            TRACEF("ARDUCAM in standby mode\n");
    }
    // enster stop mode
    else if (mode == ARDUCAM_STOP_MODE){

        // disable the LDO
        clear_bit(device,ARDUCAM_GPIO,ARDUCAM_GPIO_PWREN_MASK);
        thread_sleep(10);
        ret=get_bit(device,ARDUCAM_GPIO,ARDUCAM_GPIO_PWREN_MASK);
        if(ret==0)
            TRACEF("ARDUCAM in Stop mode\n");

    }
    else
        TRACEF("No sleep mode match\n");
}

static void wakeup(arducam_mini5mp_device_t * device, int mode){

    int ret;
    // exit standby mode
        if(mode == ARDUCAM_STANDBY_MODE){
        clear_bit(device,ARDUCAM_GPIO,ARDUCAM_GPIO_PWDN_MASK);
        thread_sleep(10);
        ret=get_bit(device,ARDUCAM_GPIO,ARDUCAM_GPIO_PWDN_MASK);
        if(ret==0)
            TRACEF("ARDUCAM wake up from standby mode\n");
    }
    // exit stop mode
    else if(mode == ARDUCAM_STOP_MODE){
        // enable the LDO
        set_bit(device,ARDUCAM_GPIO,ARDUCAM_GPIO_PWREN_MASK);
        thread_sleep(10);
        ret=get_bit(device,ARDUCAM_GPIO,ARDUCAM_GPIO_PWREN_MASK);
        if(ret==4)
            TRACEF("ARDUCAM wake up from Stop mode\n");

    }
    else 
        TRACEF("No sleep mode match\n");

}

/*----------------------*/

int arducam_trig_image_5mp(arducam_mini5mp_device_t * device) {
    int image_length = 0;
    int check_image_done = 0;

    arducam_flush_fifo(device);

    arducam_start_capture(device);

    while(!get_bit(device, ARDUCAM_TRIG , ARDUCAM_CAP_DONE_MASK)){
        thread_sleep(100);
        if (check_image_done > 10)
            return -1;
        check_image_done++;
    }
    
    image_length = arducam_fifo_length(device);


    return image_length;
}

void arducam_start_read_image_5mp(arducam_mini5mp_device_t * device) {
    int ret;
    uint8_t tmp_write;

    /* spi cam select */
    gpio_set(device->spi_nss_pin, 0);

    /* get first dummy byte, only for OV5642 */
    tmp_write = ARDUCAM_BURST_FIFO_READ;
    ret = spi_transmit(device->spi_bus_id, &tmp_write, 1);
    if (ret) TRACEF("Error to write over spi, code: %d\n", ret);

    thread_sleep(10);
}

void arducam_read_image_5mp(arducam_mini5mp_device_t * device,
                        uint8_t *buffer,
                        uint16_t length) {
    int ret, index;
    uint8_t tmp_write;
    uint8_t tmp_val;
    uint32_t delay;

    for (index=0; index<length; index++) {
        ret = spi_receive(device->spi_bus_id, &tmp_val, 1);
        if (ret) TRACEF("Error to read over spi, code: %d\n", ret);
        *buffer = tmp_val;
        buffer++;
        for (delay=0; delay<50; delay++) {
            asm("NOP");
        }
    }
}

void arducam_stop_read_image_5mp(arducam_mini5mp_device_t * device) {
    thread_sleep(10);
    gpio_set(device->spi_nss_pin, 1);
}


void arducam_enter_sleep_mode_5mp(arducam_mini5mp_device_t * device, int mode){
    sleep_mode(device,mode);
}

void arducam_exit_sleep_mode_5mp(arducam_mini5mp_device_t * device, int mode){
    wakeup(device,mode);
}

char *arducam_get_strRes_5mp(arducam_mini5mp_device_t * device, uint8_t res) {
    return cam5mp_res[res];

}

int arducam_module_init_5mp(arducam_mini5mp_device_t * device) {
    uint8_t read_test_reg = 0;
    uint8_t i, id_H, id_L;

    // Reset the CPLD
    spi_cam_write(device, 0x07, 0x80);
    thread_sleep(100);
    spi_cam_write(device, 0x07, 0x00);
    thread_sleep(100);

    for (i=0; i<3; i++) {
        spi_cam_write(device, ARDUCAM_TEST_REG, 0x12);
        read_test_reg = spi_cam_read(device, ARDUCAM_TEST_REG);

        if (read_test_reg == 0x12) {
            TRACEF("Camera detected, SPI OK!\n");
            break;
        }
        else {
            TRACEF("Camera not detected, SPI KO !, retry %d\n", i);
        }
    }

    /* Check the sensor */
    i2c_cam_write_16(device, 0xff, 0x01);
    i2c_cam_read_16(device, OV5642_CHIPID_HIGH, &id_H);
    i2c_cam_read_16(device, OV5642_CHIPID_LOW, &id_L);

    if((id_H == 0x56) && (id_L == 0x42) && (read_test_reg == 0x12)) {
        TRACEF("Camera initialisation OK !\n");
    }
    else {
        TRACEF("Camera initialisation FAIL !!!\n");
        return -1;
    }

    i2c_cam_write_16(device, 0x3008, 0x80);
    thread_sleep(200);
    i2c_cam_buf_16(device, OV5642_QVGA_Preview);
    thread_sleep(200);
    i2c_cam_buf_16(device, OV5642_JPEG_Capture_QSXGA);
    thread_sleep(200);
    i2c_cam_buf_16(device, ov5642_320x240);
    thread_sleep(200);
    i2c_cam_write_16(device, 0x3818, 0xa8);
    i2c_cam_write_16(device, 0x3621, 0x10);
    i2c_cam_write_16(device, 0x3801, 0xb0);
    i2c_cam_write_16(device, 0x4407, 0x08);
    i2c_cam_write_16(device, 0x5888, 0x00);
    i2c_cam_write_16(device, 0x5000, 0xFF);
    thread_sleep(200);

    spi_cam_write(device, ARDUCAM_ARDUCHIP_TIM, ARDUCAM_VSYNC_LEVEL_MASK);   //VSYNC is active HIGH
    arducam_setJpegSize_5mp(device, CAM_RES_640x480);
    thread_sleep(1000);
    arducam_flush_fifo(device);
    spi_cam_write(device, ARDUCAM_FRAMES, 0x00);

    return 0;
}

void arducam_mini5mp_init(arducam_mini5mp_device_t * device,
                          uint8_t spi_bus,
                          uint16_t spi_cs_pin,
                          uint8_t i2c_bus,
                          uint8_t i2c_device_address)
{
    device->spi_bus_id      = spi_bus;
    device->spi_nss_pin     = spi_cs_pin;
    device->i2c_bus_id      = i2c_bus;
    device->i2c_address     = i2c_device_address;
    device->init_cam        = &arducam_module_init_5mp;
    device->set_jpegSize    = &arducam_setJpegSize_5mp;
    device->trig_image      = &arducam_trig_image_5mp;
    device->start_read_img  = &arducam_start_read_image_5mp;
    device->read_img        = &arducam_read_image_5mp;
    device->stop_read_img   = &arducam_stop_read_image_5mp;
    device->get_strRes      = &arducam_get_strRes_5mp;
    device->sleep           = &arducam_enter_sleep_mode_5mp;
    device->wakeup          = &arducam_exit_sleep_mode_5mp;

}
