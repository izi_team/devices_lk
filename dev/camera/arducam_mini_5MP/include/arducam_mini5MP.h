#ifndef _DEV_CAMERA_ARDUCAM_MINI5MP_H_
#define _DEV_CAMERA_ARDUCAM_MINI5MP_H_

#define ARDUCAM_TEST_REG    0x00
#define ARDUCAM_WRITE_BIT   0x80

#define ARDUCAM_BMP         0
#define ARDUCAM_JPEG        1
#define ARDUCAM_RAW         2

#define CAM_RES_320x240     0
#define CAM_RES_640x480     1
#define CAM_RES_1024x768    2
#define CAM_RES_1280x960    3
#define CAM_RES_1600x1200   4
#define CAM_RES_2048x1536   5
#define CAM_RES_2592x1944   6

#define ARDUCAM_FIFO                0x04
#define ARDUCAM_FIFO_CLEAR_MASK     0x01
#define ARDUCAM_FIFO_START_MASK     0x02
#define ARDUCAM_FIFO_RDPTR_RST_MASK 0x10
#define ARDUCAM_FIFO_WRPTR_RST_MASK 0x20

#define ARDUCAM_GPIO                0x06
#define ARDUCAM_GPIO_RESET_MASK     0x01    //0 = Sensor reset, 1 =  Sensor normal operation
#define ARDUCAM_GPIO_PWDN_MASK      0x02    //0 = Sensor normal operation,  1 = Sensor standby
#define ARDUCAM_GPIO_PWREN_MASK     0x04    //0 = Sensor LDO disable, 1 = sensor LDO enable

#define ARDUCAM_BURST_FIFO_READ     0x3C    //Burst FIFO read operation
#define ARDUCAM_SINGLE_FIFO_READ    0x3D    //Single FIFO read operation

#define ARDUCAM_ARDUCHIP_REV        0x40    //ArduCHIP revision
#define ARDUCAM_VER_LOW_MASK        0x3F
#define ARDUCAM_VER_HIGH_MASK       0xC0

#define ARDUCAM_TRIG                0x41    //Trigger source
#define ARDUCAM_VSYNC_MASK          0x01
#define ARDUCAM_SHUTTER_MASK        0x02
#define ARDUCAM_CAP_DONE_MASK       0x08

#define ARDUCAM_FIFO_SIZE1          0x42    //Camera write FIFO size[7:0] for burst to read
#define ARDUCAM_FIFO_SIZE2          0x43    //Camera write FIFO size[15:8]
#define ARDUCAM_FIFO_SIZE3          0x44    //Camera write FIFO size[18:16]

#define ARDUCAM_ARDUCHIP_TIM        0x03    //Timming control
#define ARDUCAM_VSYNC_LEVEL_MASK    0x02    //0 = High active ,   1 = Low active
#define ARDUCAM_FRAMES              0x01

/* Define possible sleep mode */
#define ARDUCAM_STOP_MODE           0
#define ARDUCAM_STANDBY_MODE        1   

typedef struct arducam_mini5mp_device {
    uint8_t spi_bus_id;
    uint16_t spi_nss_pin;
    uint8_t i2c_bus_id;
    uint8_t i2c_address;
    int     (*init_cam)(struct arducam_mini5mp_device *);
    void    (*set_jpegSize)(struct arducam_mini5mp_device *, uint8_t);
    int     (*trig_image)(struct arducam_mini5mp_device *);
    void    (*start_read_img)(struct arducam_mini5mp_device *);
    void    (*read_img)(struct arducam_mini5mp_device *, uint8_t *, uint16_t);
    void    (*stop_read_img)(struct arducam_mini5mp_device *);
    char    (*get_strRes)(struct arducam_mini5mp_device *, uint8_t);
    void    (*sleep)(struct arducam_mini5mp_device *, int);
    void    (*wakeup)(struct arducam_mini5mp_device *, int);
} arducam_mini5mp_device_t;

struct sensor_reg {
    uint16_t reg;
    uint16_t val;
};

void arducam_mini5mp_init(arducam_mini5mp_device_t * device,
                          uint8_t spi_bus,
                          uint16_t spi_cs_pin,
                          uint8_t i2c_bus,
                          uint8_t i2c_device_address);

#endif  // _DEV_CAMERA_ARDUCAM_MINI5MP_H_
