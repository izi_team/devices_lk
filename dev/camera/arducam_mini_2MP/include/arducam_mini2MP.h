#ifndef _DEV_CAMERA_ARDUCAM_MINI2MP_H_
#define _DEV_CAMERA_ARDUCAM_MINI2MP_H_

#define ARDUCAM_TEST_REG    0x00
#define ARDUCAM_WRITE_BIT   0x80

#define CAM_RES_160x120     0
#define CAM_RES_176x144     1
#define CAM_RES_320x240     2
#define CAM_RES_352x288     3
#define CAM_RES_640x480     4
#define CAM_RES_800x600     5
#define CAM_RES_1024x768    6
#define CAM_RES_1280x1024   7
#define CAM_RES_1600x1200   8

#define ARDUCAM_FIFO                0x04
#define ARDUCAM_FIFO_CLEAR_MASK     0x01
#define ARDUCAM_FIFO_START_MASK     0x02
#define ARDUCAM_FIFO_RDPTR_RST_MASK 0x10
#define ARDUCAM_FIFO_WRPTR_RST_MASK 0x20

#define ARDUCAM_GPIO                0x06
#define ARDUCAM_GPIO_RESET_MASK     0x01    //0 = Sensor reset, 1 =  Sensor normal operation
#define ARDUCAM_GPIO_PWDN_MASK      0x02    //0 = Sensor normal operation,  1 = Sensor standby
#define ARDUCAM_GPIO_PWREN_MASK     0x04    //0 = Sensor LDO disable, 1 = sensor LDO enable

#define ARDUCAM_BURST_FIFO_READ     0x3C    //Burst FIFO read operation
#define ARDUCAM_SINGLE_FIFO_READ    0x3D    //Single FIFO read operation

#define ARDUCAM_ARDUCHIP_REV        0x40    //ArduCHIP revision
#define ARDUCAM_VER_LOW_MASK        0x3F
#define ARDUCAM_VER_HIGH_MASK       0xC0

#define ARDUCAM_TRIG                0x41    //Trigger source
#define ARDUCAM_VSYNC_MASK          0x01
#define ARDUCAM_SHUTTER_MASK        0x02
#define ARDUCAM_CAP_DONE_MASK       0x08

#define ARDUCAM_FIFO_SIZE1          0x42    //Camera write FIFO size[7:0] for burst to read
#define ARDUCAM_FIFO_SIZE2          0x43    //Camera write FIFO size[15:8]
#define ARDUCAM_FIFO_SIZE3          0x44    //Camera write FIFO size[18:16]


typedef struct arducam_mini2mp_device {
    uint8_t spi_bus_id;
    uint16_t spi_nss_pin;
    uint8_t i2c_bus_id;
    uint8_t i2c_address;
    int     (*init_cam)(struct arducam_mini2mp_device *);
    void    (*init_optic)(struct arducam_mini2mp_device *);
    void    (*set_jpegSize)(struct arducam_mini2mp_device *, uint8_t);
    int     (*trig_image)(struct arducam_mini2mp_device *);
    void    (*read_image)(struct arducam_mini2mp_device *, uint8_t *, uint16_t);
    char    (*get_strRes)(struct arducam_mini2mp_device *, uint8_t);
} arducam_mini2mp_device_t;

struct sensor_reg {
    uint8_t reg;
    uint8_t val;
};

void arducam_mini2mp_init(arducam_mini2mp_device_t * device,
                          uint8_t spi_bus,
                          uint16_t spi_cs_pin,
                          uint8_t i2c_bus,
                          uint8_t i2c_device_address);

#endif  // _DEV_CAMERA_ARDUCAM_MINI2MP_H_
