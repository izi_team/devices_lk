/*
 * Copyright (c) 2022 Mihail CHERCIU
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <stdlib.h>
#include <app.h>
#include <string.h>
#include <dev/i2c.h>
#include <dev/gpio.h>
#include <lk/trace.h>
#include <kernel/thread.h>
#include <platform/spi.h>
#include "arducam_mini2MP.h"
#include "ov2640.h"

#define LOCAL_TRACE 1

#define CAM_NR_RES          10
const char *cam2mp_res[CAM_NR_RES] = {
    "160x120",
    "176x144",
    "320x240",
    "352x288",
    "640x480",
    "800x600",
    "1024x768",
    "1280x1024",
    "1600x1200",
    "RES_ERROR"
};

static void spi_cam_write(arducam_mini2mp_device_t *device,
                          uint8_t address,
                          uint8_t data) {
    int ret;
    uint8_t buf[2];

    buf[0] = address | ARDUCAM_WRITE_BIT;
    buf[1] = data;

    gpio_set(device->spi_nss_pin, 0);

    /* Write */
    ret = spi_transmit(device->spi_bus_id, buf, 2);
    if (ret) TRACEF("Error to write over spi, code: %d\n", ret);

    gpio_set(device->spi_nss_pin, 1);
}

static uint8_t spi_cam_read(arducam_mini2mp_device_t *device,
                            uint8_t address) {
    int ret;
    uint8_t value = 0;
    uint8_t addr;

    addr = address & 0x7f;

    gpio_set(device->spi_nss_pin, 0);

    /* Write */
    ret = spi_transmit(device->spi_bus_id, &addr, 1);
    if (ret) TRACEF("Error to write over spi, code: %d\n", ret);

    /* Read */
    ret = spi_receive(device->spi_bus_id, &value, 1);
    if (ret) TRACEF("Error to read over spi, code: %d\n", ret);

    gpio_set(device->spi_nss_pin, 1);

    return value;
}

static void i2c_cam_read(arducam_mini2mp_device_t *device,
                         uint8_t writeReg,
                         uint8_t *readReg) {
    int ret;

    ret = i2c_transmit(device->i2c_bus_id, device->i2c_address, &writeReg, 1);
    if (ret) TRACEF("Error to write over i2c, code: %d\n", ret);
    ret = i2c_receive(device->i2c_bus_id, device->i2c_address, readReg, 1);
    if (ret) TRACEF("Error to read over i2c, code: %d\n", ret);
}

static void i2c_cam_write(arducam_mini2mp_device_t *device,
                         uint8_t writeReg,
                         uint8_t writeData) {
    int ret;
    uint8_t buf[2];

    buf[0] = writeReg;
    buf[1] = writeData;

    ret = i2c_transmit(device->i2c_bus_id, device->i2c_address, buf, 2);
    if (ret) TRACEF("Error to write over i2c, code: %d\n", ret);
}

static void i2c_cam_buf(arducam_mini2mp_device_t *device,
                        const struct sensor_reg reglist[]) {

    uint8_t reg_addr = 0;
    uint8_t reg_val = 0;
    const struct sensor_reg *next = reglist;

    while ((reg_addr != 0xff) | (reg_val != 0xff))
    {
        reg_addr =next->reg;
        reg_val = next->val;
        i2c_cam_write(device, reg_addr, reg_val);
        //sleep_ms(10);
        next++;
    }
}

int arducam_module_init_2mp(arducam_mini2mp_device_t * device) {
    uint8_t read_test_reg = 0;
    uint8_t i, id_H, id_L;

    for (i=0; i<3; i++) {
        spi_cam_write(device, ARDUCAM_TEST_REG, 0x12);
        read_test_reg = spi_cam_read(device, ARDUCAM_TEST_REG);

        if (read_test_reg == 0x12) {
            TRACEF("Camera detected !\n");
            break;
        }
        else {
            TRACEF("Camera not detected !, retry %d\n", i);
        }
    }

    /* Check the sensor */
    i2c_cam_read(device, 0x0A, &id_H);
    i2c_cam_read(device, 0x0B, &id_L);

    if(id_H == 0x26 && (id_L == ARDUCAM_ARDUCHIP_REV ||id_L == 0x41 || id_L == 0x42) && read_test_reg == 0x12) {
        TRACEF("Camera initialisation OK !\n");
        return 0;
    }
    else {
        TRACEF("Camera initialisation FAIL !!!\n");
        return -1;
    }
}

void arducam_OV2640_init(arducam_mini2mp_device_t * device) {
    i2c_cam_write(device, 0xff, 0x01);
    i2c_cam_write(device, 0x12, 0x80);
    i2c_cam_buf(device, OV2640_JPEG_INIT);
    i2c_cam_buf(device, OV2640_YUV422);
    i2c_cam_buf(device, OV2640_JPEG);
    i2c_cam_write(device, 0xff, 0x01);
    i2c_cam_write(device, 0x15, 0x00);
    i2c_cam_buf(device, OV2640_320x240_JPEG);
}

void arducam_setJpegSize_2mp(arducam_mini2mp_device_t * device,
                         uint8_t size) {
    switch(size)
    {
        case CAM_RES_160x120:
            i2c_cam_buf(device, OV2640_160x120_JPEG);
            break;
        case CAM_RES_176x144:
            i2c_cam_buf(device, OV2640_176x144_JPEG);
            break;
        case CAM_RES_320x240:
            i2c_cam_buf(device, OV2640_320x240_JPEG);
            break;
        case CAM_RES_352x288:
            i2c_cam_buf(device, OV2640_352x288_JPEG);
            break;
        case CAM_RES_640x480:
            i2c_cam_buf(device, OV2640_640x480_JPEG);
            break;
        case CAM_RES_800x600:
            i2c_cam_buf(device, OV2640_800x600_JPEG);
            break;
        case CAM_RES_1024x768:
            i2c_cam_buf(device, OV2640_1024x768_JPEG);
            break;
        case CAM_RES_1280x1024:
            i2c_cam_buf(device, OV2640_1280x1024_JPEG);
            break;
        case CAM_RES_1600x1200:
            i2c_cam_buf(device, OV2640_1600x1200_JPEG);
            break;
        default:
            i2c_cam_buf(device, OV2640_320x240_JPEG);
            break;
    }
}

/* --- work with bit ---*/
static void set_bit(arducam_mini2mp_device_t * device, uint8_t addr, uint8_t val) {
    uint8_t tmp;

    tmp = spi_cam_read(device, addr);
    tmp |= val;
    spi_cam_write(device, addr, tmp);
}

static void clear_bit(arducam_mini2mp_device_t * device, uint8_t addr, uint8_t val) {
    uint8_t tmp;

    tmp = spi_cam_read(device, addr);
    tmp &= (~val);
    spi_cam_write(device, addr, tmp);
}

static uint8_t get_bit(arducam_mini2mp_device_t * device, uint8_t addr, uint8_t mask) {
    uint8_t tmp;

    tmp = spi_cam_read(device, addr);
    tmp &= mask;

    return tmp;
}
/*----------------------*/

/*-- work with buffer --*/
static void arducam_flush_fifo(arducam_mini2mp_device_t * device)
{
    spi_cam_write(device, ARDUCAM_FIFO, ARDUCAM_FIFO_CLEAR_MASK);
}

static int arducam_fifo_length(arducam_mini2mp_device_t * device)
{
    uint8_t fifo1,fifo2,fifo3;
    uint32_t len=0;

    fifo1 = spi_cam_read(device, ARDUCAM_FIFO_SIZE1);
    fifo2 = spi_cam_read(device, ARDUCAM_FIFO_SIZE2);
    fifo3 = spi_cam_read(device, ARDUCAM_FIFO_SIZE3) & 0x7f;
    len = (((uint32_t)fifo3 << 16) | ((uint32_t)fifo2 << 8) | (uint32_t)fifo1) & 0x07fffff;
    return len;
}

static void arducam_fifo_brust(arducam_mini2mp_device_t * device) {
    int ret;
    uint8_t tmp_read;
    uint8_t tmp_write;

    /* spi cam select */
    gpio_set(device->spi_nss_pin, 0);

    /* get first dummy byte, only for OV2640 */
    tmp_write = ARDUCAM_BURST_FIFO_READ;
    ret = spi_transmit(device->spi_bus_id, &tmp_write, 1);
    if (ret) TRACEF("Error to write over spi, code: %d\n", ret);

    ret = spi_receive(device->spi_bus_id, &tmp_read, 1);
    if (ret) TRACEF("Error to read over spi, code: %d\n", ret);

    gpio_set(device->spi_nss_pin, 1);
}

static void arducam_start_capture(arducam_mini2mp_device_t * device)
{
    spi_cam_write(device, ARDUCAM_FIFO, ARDUCAM_FIFO_START_MASK);
}

/*----------------------*/

int arducam_trig_image_2mp(arducam_mini2mp_device_t * device) {
    int image_length = 0;
    int check_image_done = 0;

    arducam_flush_fifo(device);

    arducam_start_capture(device);

    while(!get_bit(device, ARDUCAM_TRIG , ARDUCAM_CAP_DONE_MASK)){
        thread_sleep(100);
        if (check_image_done > 10)
            return -1;
        check_image_done++;
    }

    image_length = arducam_fifo_length(device);

    return image_length;
}

void arducam_read_image_2mp(arducam_mini2mp_device_t * device,
                        uint8_t *buffer,
                        uint16_t length) {
    int ret;
    uint8_t tmp_write;

    /* spi cam select */
    gpio_set(device->spi_nss_pin, 0);

    /* get first dummy byte, only for OV2640 */
    tmp_write = ARDUCAM_BURST_FIFO_READ;
    ret = spi_transmit(device->spi_bus_id, &tmp_write, 1);
    if (ret) TRACEF("Error to write over spi, code: %d\n", ret);

    thread_sleep(10);

    ret = spi_receive(device->spi_bus_id, buffer, length);
    if (ret) TRACEF("Error to read over spi, code: %d\n", ret);

    gpio_set(device->spi_nss_pin, 1);
}

char *arducam_get_strRes_2mp(arducam_mini2mp_device_t * device, uint8_t res) {
    return cam2mp_res[res];

}

void arducam_mini2mp_init(arducam_mini2mp_device_t * device,
                          uint8_t spi_bus,
                          uint16_t spi_cs_pin,
                          uint8_t i2c_bus,
                          uint8_t i2c_device_address)
{
    device->spi_bus_id   = spi_bus;
    device->spi_nss_pin  = spi_cs_pin;
    device->i2c_bus_id   = i2c_bus;
    device->i2c_address  = i2c_device_address;
    device->init_cam     = &arducam_module_init_2mp;
    device->init_optic   = &arducam_OV2640_init;
    device->set_jpegSize = &arducam_setJpegSize_2mp;
    device->trig_image   = &arducam_trig_image_2mp;
    device->read_image   = &arducam_read_image_2mp;
    device->get_strRes   = &arducam_get_strRes_2mp;
}
