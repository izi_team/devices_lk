#ifndef _DEV_MEMORY_24CWXX_H_
#define _DEV_MEMORY_24CWXX_H_

/* EEPROM Mapping */
#define EEPROM_24CWXX_SOFT_VERSION        0x0010

typedef struct eeprom_24cwxx_device {
    uint8_t bus_id;
    uint8_t i2c_address;
    void    (*write)(struct eeprom_24cwxx_device *, uint16_t, const void *, size_t);
    void    (*read) (struct eeprom_24cwxx_device *, uint16_t, void *, size_t);
} eeprom_24cwxx_device_t;

void eeprom_24cwxx_init(eeprom_24cwxx_device_t * device,
                        uint8_t bus,
                        uint8_t device_address);

#endif  // _DEV_MEMORY_24CWXX_H_
