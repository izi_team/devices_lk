/*
 * Copyright (c) 2020 Mihail CHERCIU
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <stdlib.h>
#include <app.h>
#include <string.h>
#include <dev/i2c.h>
#include <lk/trace.h>
#include "24cwxx.h"

#define LOCAL_TRACE 0

void eeprom_24cwxx_write(eeprom_24cwxx_device_t * device,
                         uint16_t memory_address,
                         const void *data,
                         size_t len) {
    int ret;

    size_t length = len + 2; //mem address(2 bytes) + data

    uint8_t *mem_data = malloc(length);
    if (!mem_data) {
        TRACEF("Fail to allocate memory");
        return;
    }

    mem_data[0] = (uint8_t)(memory_address >> 8);
    mem_data[1] = (uint8_t)(memory_address & 0x00FF);
    memcpy(&mem_data[2], data, len);

    ret = i2c_transmit(device->bus_id, device->i2c_address, mem_data, length);
    if (ret) TRACEF("Write EEPROM fail: %d\n", ret);
}

void eeprom_24cwxx_read(eeprom_24cwxx_device_t * device,
                        uint16_t memory_address,
                        void *data,
                        size_t len) {
    int ret;
    char mem_add[2];

    mem_add[0] = (uint8_t)(memory_address >> 8);;
    mem_add[1] = (uint8_t)(memory_address & 0x00FF);

    ret = i2c_transmit(device->bus_id, device->i2c_address, mem_add, 2);
    if (ret) TRACEF("Write EEPROM fail: %d\n", ret);

    ret = i2c_receive(device->bus_id, device->i2c_address, data, len);
    if (ret) TRACEF("Read EEPROM fail: %d\n", ret);
}

void eeprom_24cwxx_init(eeprom_24cwxx_device_t * device,
                        uint8_t bus,
                        uint8_t device_address)
{
    device->bus_id       = bus;
    device->i2c_address  = device_address;
    device->write        = &eeprom_24cwxx_write;
    device->read         = &eeprom_24cwxx_read;
}
