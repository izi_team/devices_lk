#ifndef _DEV_MEMORY_W25XX0CL_H_
#define _DEV_MEMORY_W25XX0CL_H_

/* FLASH registers mapping */
#define FLASH_W25XX0CL_REG_WRITE_STATUS_REG         0x01
#define FLASH_W25XX0CL_REG_PAGE_PROGRAM             0x02
#define FLASH_W25XX0CL_REG_READ_DATA                0x03
#define FLASH_W25XX0CL_REG_WRITE_DISABLE            0x04
#define FLASH_W25XX0CL_REG_READ_STATUS_REG          0x05
#define FLASH_W25XX0CL_REG_WRITE_EN                 0x06
#define FLASH_W25XX0CL_REG_WRITE_EN_FOR_VOLATILE    0x50
#define FLASH_W25XX0CL_REG_DEVICE_ID                0xAB
#define FLASH_W25XX0CL_REG_JEDEC_ID                 0x9F
#define FLASH_W25XX0CL_REG_ERASE                    0xC7
#define FLASH_W25XX0CL_REG_BLOCK_ERASE              0xD8

/* FLASH JEDEC ID */
#define FLASH_W25XX0CL_ID_MANUFACTURE               0xEF
#define FLASH_W25XX0CL_DEVICE_ID                    0x12

/* FLASH PAGE/SECTOR/BLOCK */
#define FLASH_W25XX0CL_PAGE_SIZE                    256

typedef struct flash_w25xx0cl_device {
    uint8_t  bus_id;
    uint16_t nss_pin;
    void     (*write)(struct flash_w25xx0cl_device *, uint32_t, const void *, size_t);
    void     (*read)(struct flash_w25xx0cl_device *, uint32_t, const void *, size_t);
    void     (*erase)(struct flash_w25xx0cl_device *);
    void     (*block_erase)(struct flash_w25xx0cl_device *, uint32_t);
    u_int8_t (*get_id)(struct flash_w25xx0cl_device *);
} flash_w25xx0cl_device_t;

void flash_w25xx0cl_init(flash_w25xx0cl_device_t * device,
                        uint8_t bus,
                        uint16_t cs_pin);

#endif  // _DEV_MEMORY_W25XX0CL_H_
