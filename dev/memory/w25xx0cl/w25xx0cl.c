/*
 * Copyright (c) 2020 Mihail CHERCIU
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <stdlib.h>
#include <app.h>
#include <string.h>
#include <dev/gpio.h>
#include <platform/spi.h>
#include <lk/trace.h>
#include "w25xx0cl.h"

#define LOCAL_TRACE 0

static void flash_w25xx0cl_write_enable(flash_w25xx0cl_device_t * device) {
    int ret;
    uint8_t buffer[1];

    buffer[0] = FLASH_W25XX0CL_REG_WRITE_EN;

    gpio_set(device->nss_pin, 0);
    spi_transmit(device->bus_id, buffer, 1);
    if (ret) TRACEF("Write to FLASH fail: %d\n", ret);
    gpio_set(device->nss_pin, 1);
}

static void flash_w25xx0cl_write(flash_w25xx0cl_device_t * device,
                                 uint32_t memory_address,
                                 const void *data,
                                 size_t len) {

    int ret;
    uint32_t nrByteOnePage;
    int write_len, memAddOffset;
    size_t length_buffer = FLASH_W25XX0CL_PAGE_SIZE + 3 +1; //page size + instruction(1 byte) + mem address(3 bytes) + data

    nrByteOnePage = 256 - (memory_address % 256); // calculate remaining bytes to write on start memory address page
    memAddOffset = 0;

    uint8_t *send_data = malloc(length_buffer);
    if (!send_data) {
        TRACEF("Fail to allocate memory");
        return;
    }

    do {
        write_len = (len <= nrByteOnePage) ? len : nrByteOnePage;

        /* Prepare buffer */
        send_data[0] = (uint8_t)(FLASH_W25XX0CL_REG_PAGE_PROGRAM);
        send_data[1] = (uint8_t)((memory_address >> 16) & 0xFF);
        send_data[2] = (uint8_t)((memory_address >> 8) & 0xFF);
        send_data[3] = (uint8_t)(memory_address & 0xFF);
        memcpy(&send_data[4], data + memAddOffset, write_len);

        flash_w25xx0cl_write_enable(device);

        /* Write buffer */
        gpio_set(device->nss_pin, 0);
        spi_transmit(device->bus_id, send_data, write_len + 4);
        if (ret) TRACEF("Write to FLASH fail: %d\n", ret);
        gpio_set(device->nss_pin, 1);

        memory_address += write_len;
        memAddOffset += write_len;
        len -= write_len;
        nrByteOnePage = FLASH_W25XX0CL_PAGE_SIZE;
    } while (len > 0);

    free(send_data);
}

static void flash_w25xx0cl_read(flash_w25xx0cl_device_t * device,
                                uint32_t memory_address,
                                void *data,
                                size_t len) {

    int ret;
    uint8_t send_data[4];

    send_data[0] = (uint8_t)(FLASH_W25XX0CL_REG_READ_DATA);
    send_data[1] = (uint8_t)((memory_address >> 16) & 0xFF);
    send_data[2] = (uint8_t)((memory_address >> 8) & 0xFF);
    send_data[3] = (uint8_t)(memory_address & 0xFF);

    gpio_set(device->nss_pin, 0);
    ret = spi_transmit(device->bus_id, send_data, 4);
    if (ret) TRACEF("Write to FLASH fail: %d\n", ret);
    ret = spi_receive(device->bus_id,(uint8_t *) data, len);
    if (ret) TRACEF("Read from FLASH fail: %d\n", ret);
    gpio_set(device->nss_pin, 1);
}

static void flash_w25xx0cl_erase(flash_w25xx0cl_device_t * device) {

    int ret;
    uint8_t send_data[1];

    flash_w25xx0cl_write_enable(device);

    send_data[0] = (uint8_t)(FLASH_W25XX0CL_REG_ERASE);

    gpio_set(device->nss_pin, 0);
    spi_transmit(device->bus_id, send_data, 1);
    if (ret) TRACEF("Write to FLASH fail: %d\n", ret);
    gpio_set(device->nss_pin, 1);
}

static void flash_w25xx0cl_block_erase(flash_w25xx0cl_device_t * device,
                                       uint32_t memory_address) {

    int ret;
    uint8_t send_data[4];

    flash_w25xx0cl_write_enable(device);

    send_data[0] = (uint8_t)(FLASH_W25XX0CL_REG_BLOCK_ERASE);
    send_data[1] = (uint8_t)((memory_address >> 16) & 0xFF);
    send_data[2] = (uint8_t)((memory_address >> 8) & 0xFF);
    send_data[3] = (uint8_t)(memory_address & 0xFF);

    gpio_set(device->nss_pin, 0);
    ret = spi_transmit(device->bus_id, send_data, 4);
    if (ret) TRACEF("Write to FLASH fail: %d\n", ret);
    gpio_set(device->nss_pin, 1);
}

static u_int8_t flash_w25xx0cl_get_id(flash_w25xx0cl_device_t * device) {
    int ret;
    uint8_t send_data[1];
    uint8_t rec_data[3];
    u_int8_t flash_id = 0;

    send_data[0] = (uint8_t)(FLASH_W25XX0CL_REG_JEDEC_ID);

    gpio_set(device->nss_pin, 0);
    ret = spi_transmit(device->bus_id, send_data, 1);
    if (ret) TRACEF("Write to FLASH fail: %d\n", ret);
    ret = spi_receive(device->bus_id,(uint8_t *) rec_data, 3);
    if (ret) TRACEF("Read from FLASH fail: %d\n", ret);
    gpio_set(device->nss_pin, 1);

    LTRACEF("rec_data: %p\n", rec_data[0]);
    LTRACEF("rec_data: %p\n", rec_data[1]);
    LTRACEF("rec_data: %p\n", rec_data[2]);

    flash_id = rec_data[0];
    if (flash_id == 0xEF) {
        printf("Winbond Serial Flash\n");
    }
    else {
        TRACEF("Identification FAILED !");
    }

    return flash_id;
}

void flash_w25xx0cl_init(flash_w25xx0cl_device_t * device,
                        uint8_t bus,
                        uint16_t cs_pin)
{
    device->bus_id       = bus;
    device->nss_pin      = cs_pin;
    device->write        = &flash_w25xx0cl_write;
    device->read         = &flash_w25xx0cl_read;
    device->erase        = &flash_w25xx0cl_erase;
    device->block_erase  = &flash_w25xx0cl_block_erase;
    device->get_id       = &flash_w25xx0cl_get_id;
}