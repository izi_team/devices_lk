#ifndef _DEV_MOTOR_ENCODER_AS5048A_H_
#define _DEV_MOTOR_ENCODER_AS5048A_H_

/* AS5048A Registers */
#define AS5048A_NOP 0x0000
#define AS5048A_CLEAR_ERROR_FLAG 0x0001
#define AS5048A_PROGRAMMING_CONTROL 0x0003
#define AS5048A_OTP_REGISTER_ZERO_POSITION_HI 0x0016
#define AS5048A_OTP_REGISTER_ZERO_POSITION_LOW 0x0017
#define AS5048A_DIAGNOSTICS_PLUS_AUTOMATIC_GAIN_CONTROL 0x3FFD
#define AS5048A_MAGNITUDE 0x3FFE
#define AS5048A_ANGLE 0x3FFF

typedef struct
{
    uint16_t magnitude;
    uint16_t angle;
    bool     errorFlag;
} motor_encoder_t;

typedef struct motor_encoder_as5048a_device
{
    uint8_t bus_id;
    uint16_t nss_pin;
    motor_encoder_t encoder;
    //14-bit output from the encoder
    void (*get_value)(struct motor_encoder_as5048a_device *);
    void (*write_value)(struct motor_encoder_as5048a_device *, uint16_t reg, uint16_t data);
} motor_encoder_as5048a_device_t;

void motor_encoder_as5048a_init(motor_encoder_as5048a_device_t *device,
                                uint8_t bus,
                                uint16_t cs_pin);

#endif
