/*
 * Copyright (c) 2021 Mihail CHERCIU
 *               2022 Aihua ZHANG
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <stdlib.h>
#include <app.h>
#include <dev/gpio.h>
#include <kernel/thread.h>
#include <platform/spi.h>
#include <lk/trace.h>
#include "as5048a.h"

static uint16_t motor_encoder_as5048a_read(motor_encoder_as5048a_device_t *device, uint16_t reg)
{

    int ret;
    uint8_t send_data[2];
    uint8_t rec_data[2];
    uint16_t result = 0;

    rec_data[0] = 0x00;
    rec_data[1] = 0x00;

    // set bit 14 to 1 to read the selected register
    reg |= 0x4000;
    if (__builtin_parity(reg))
    {
        reg |= 0x8000;
    }
    else
    {
        reg &= 0x7FFF;
    }
    send_data[1] = (uint8_t)reg;
    send_data[0] = (uint8_t)(reg >> 8);
    // chip select
    gpio_set(device->nss_pin, 0);
    // choose the wanted register
    ret = spi_transmit(device->bus_id, send_data, 2);
    // chip release
    gpio_set(device->nss_pin, 1);
    if (ret)
        TRACEF("Write to FLASH fail: %d\n", ret);

    // chip select
    gpio_set(device->nss_pin, 0);
    // get the data from the selected register
    ret = spi_receive(device->bus_id, rec_data, 2);
    // chip release
    gpio_set(device->nss_pin, 1);
    if (ret)
        TRACEF("Read from FLASH fail: %d\n", ret);
    // check the error flag before reading
    if ((rec_data[0] & 0x40) != 0)
    {
        TRACEF("Error when transmitting the register: %x\n", reg);
        device->encoder.errorFlag = true;
    }
    else
    {
        device->encoder.errorFlag = false;
    }

    result = (((uint16_t)rec_data[0] << 8) | rec_data[1]) & ~0xC000;

    return result;
}

static void motor_encoder_as5048a_write(motor_encoder_as5048a_device_t *device, uint16_t reg, uint16_t data)
{

    int ret;
    uint8_t send_data[2];
    uint8_t rec_data[2];

    rec_data[0] = 0x00;
    rec_data[1] = 0x00;

    // choose the wanted register
    reg |= 0x4000;
    if (__builtin_parity(reg))
    {
        reg |= 0x8000;
    }
    else
    {
        reg &= 0x7FFF;
    }
    send_data[1] = (uint8_t)reg;
    send_data[0] = (uint8_t)(reg >> 8);
    gpio_set(device->nss_pin, 0);
    ret = spi_transmit(device->bus_id, send_data, 2);
    if (ret)
        TRACEF("Write to FLASH fail: %d\n", ret);
    gpio_set(device->nss_pin, 1);

    // Load data
    send_data[1] = (uint8_t)(data >> 8);
    send_data[0] = (uint8_t)data;
    gpio_set(device->nss_pin, 0);
    ret = spi_transmit(device->bus_id, send_data, 2);
    gpio_set(device->nss_pin, 1);
    if (ret)
        TRACEF("Write to FLASH fail: %d\n", ret);

    // send NOP command
    send_data[1] = (AS5048A_NOP >> 8) | 0xC0;
    send_data[0] = 0x00;
    gpio_set(device->nss_pin, 0);
    ret = spi_xfer(device->bus_id, send_data, rec_data, 2);
    if (ret)
        TRACEF("Write to FLASH fail: %d\n", ret);
    if (((((uint16_t)rec_data[0] << 8) | rec_data[1]) & ~0xC000) != data)
    {
        TRACEF("Error when transmitting the data: %d\n", data);
        device->encoder.errorFlag = true;
    }
    else
    {
        device->encoder.errorFlag = false;
    }
    gpio_set(device->nss_pin, 1);
}

static void motor_encoder_as5048a_get_value(motor_encoder_as5048a_device_t *device)
{
    device->encoder.angle = motor_encoder_as5048a_read(device, AS5048A_ANGLE);
    device->encoder.magnitude = motor_encoder_as5048a_read(device, AS5048A_MAGNITUDE);
}

static void motor_encoder_as5048a_write_value (motor_encoder_as5048a_device_t *device, uint16_t reg, uint16_t data) 
{
    motor_encoder_as5048a_write(device, reg, data);
}

void motor_encoder_as5048a_init(motor_encoder_as5048a_device_t *device,
                                uint8_t bus,
                                uint16_t cs_pin)
{

    device->bus_id = bus;
    device->nss_pin = cs_pin;
    device->get_value = &motor_encoder_as5048a_get_value;
    device->write_value = &motor_encoder_as5048a_write_value;
}
